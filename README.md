# Quack Compiler # 

### A graduate project by Ryan Leonard ###

## How to use this compiler? ##

Either generating code then running it:

    ./quack ./good/RecursionTests.qk
    cd runtime
    make
    ./runtime

Or use the automated codegen-and-run script

    ./run ./good/RecursionTests.qk

## The Good: working quack programs! ##

    $ ./run ./good/

Will execute all of the good working quack programs.
The output of each will be printed

* DispatchTests.qk demonstrates successful dynamic dispatch
* ControlFlowTests.qk demonstrates successful use of control flow and short circuit evaluation
* ExpressionTests.qk demonstrates successful execution of all varieties of expressions.
* RecursionTests.qk demonstrates successful recursive test cases

## The Bad: source codes that are not quack programs and correctly caught by static checker ##

    $ ./run ./bad/

Each of these test cases is described by the file name.
When running the bad test cases using the 'run' script, rather than attempt to execute the .exe file, it will instead cat whatever was printed to stderr.

## The Ugly: source codes that are not quack programs but *not* caught by static checker ##

Source codes that are not quack programs should not have their code generated.
This is not the case for all of the source files found in ./bad/broken.
Each of these source files has a discussion of why they *should* raise an error at staticcheck time.
They do not raise an error because our compiler is missing a handful of static checks.

## Discussion of Quack Modifications ##

I do not know of any regressions that I had in my compiler over the course of this term.

I did have the following modifications/assumptions:

* Assumption: Quack programmer will use C-style declerations of variables.

    I.e. when static checking, we only ensure that there is a decleration in the same method, we do not ensure that the decleration comes before the usage.
    I do have the Data Flow Analysis framework put together in src/ivs.py, which returns 'initialized variables' and 'possibly inititalized variables'.
    I did attempt to have a checker to ensure no usage before initialization. 
    See [ivs.py](https://bitbucket.org/terminalstderr/quackproglang/raw/4fdd9b41ae5286629e0cd89e69e910919b3aed6d/src/ivs.py).

* Assumption: Quack programmer will declare type of all variables.

    I.e. type inference is not implemented.