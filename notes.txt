ASSUMPTION: C style declerations of variables (at the top of the block)
ASSUMPTION: When overridding a class, attributes will be overridden in same order as parent class
ASSUMPTION:

Things to do for static checker:
# Building the tree
1. Build class hierarchy (SymbolTable)

2.1 For each class: Build variable table (Vector of attributes)
    a. every subclass inherits the attributes of the super class
    (DON'T TRY TO CHECK CONTRA/COUNTERVARIANCE HERE!)
2.2 For each class: Build method table (Vector of methods)
    a. every subclass inherits the methods of the super class
    (DON'T TRY TO CHECK CONTRA/COUNTERVARIANCE HERE!)
3. For each method: get all initialized variables
4. For each method: infer the type of all of the initialized variables

# Looking through the tree
5. check counter/contravariance of methods
    a. This does not require looking at the list of statements or any expressions, only have to look at the declerations

# Looking through the statements for every method
6. Check if there are any calls to undefined methods
7. Check if there are any usages of uninitialized variables
    a. We will assume a c-style initialization of variables, so we don't have to do a Dataflow analysis
8. Check if/while conditional expressions are type Boolean
9. Check invokation of methods have the correct argument types
    a. notice that the invoke-args can be subtypes of declared argument types
10. Check return statements expressions have the correct return type
    a. notice that the return-exp can be subtype of declared return type
11. All assignment statements that have a path-exp, the path-exp must be of 'this-class' type

Question: Will lists always be terminated with a "None" object? Or when building a list can we just ommit any Nones? In that case we should represent an empty list as an empty list instead of a list with [None].
We handle lists as python lists. --> plural implies list
We handle empty as python 'None'

-- Anotated Grammer --
Program(classes, statmenets)
program 				: classlist statementlist 

[]
[Class, Class, Class, ...]
classlist 			: classlist class 
								| 

[]
[Statement, Statement, ...]
statementlist 	: statementlist statement
                | 

Class(signature, body)
class 				: class_sig class_body 

Signature(ident, arguments, parent)
class_sig 		: CLASS IDENT LPAREN arglist RPAREN
              | CLASS IDENT LPAREN arglist RPAREN EXTENDS IDENT
              | CLASS IDENT LPAREN arglist RPAREN EXTENDS T_OBJ

Body(statements, methods)
class_body : LBRACKET statementlist methodlist RBRACKET

[]
[arg, arg, arg, ...]
arglist 		: arglist SEP arg
            | arg
            | 

Argument(ident, type)
arg 		: IDENT TYPE IDENT
        | IDENT TYPE T_INT 
        | IDENT TYPE T_OBJ 
        | IDENT TYPE T_BOOLEAN
        | IDENT TYPE T_STRING 
[]
[method, method, method, ...]
methodlist : methodlist method
           | 

Method(ident, args, block)
method  : DEF IDENT LPAREN arglist RPAREN statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE IDENT statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_INT statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_OBJ statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_BOOLEAN statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_STRING statementblock 

Block(statements)
statementblock 	: LBRACKET statementlist RBRACKET 

Statement -> condition, 

Statement() (simple parent class)
statement 				: statement_if
              		| statement_while
              		| statement_return
              		| statement_assign
              		| statement_rexp

StatementIf(condition_exp, block, eliflist, else_block=None)
statement_if 			: IF rexp statementblock eliflist
                	| IF rexp statementblock eliflist ELSE statementblock



StatementElif(condition_exp, block)
[statementElif, statementElif, statementElif]
eliflist 					: ELIF rexp statementblock
                 	| 

StatementWhile(condition_exp, block)
statement_while 	: WHILE rexp statementblock

StatementRet(return_exp=None)
statement_return : RETURN EOS
                 | RETURN rexp EOS

StatementAssign(location, expression)
statement_assign : lexp GETS rexp EOS
                 | lexp TYPE IDENT GETS rexp EOS
                 | lexp TYPE T_INT GETS rexp EOS
                 | lexp TYPE T_BOOLEAN GETS rexp EOS
                 | lexp TYPE T_OBJ GETS rexp EOS
                 | lexp TYPE T_STRING GETS rexp EOS

# Think of path_exp as being the path that indicates the path to accessing the ident
Location(ident, path_exp=none)
lexp 					: IDENT
       				| rexp ACCESS IDENT

StatementExp(expression)
statement_rexp : rexp EOS 

# TODO
Expression(value)
rexp 		: STRING
       	| NUMBER
       	| lexp
rexp 		: LPAREN rexp RPAREN

ExpressionUnaop(operation, exp1)
rexp 		: MINUS rexp 
ExpressionBinop(operation, exp1, exp2)
rexp 		: rexp DIVIDE rexp
       	| rexp TIMES rexp
rexp 		: rexp PLUS rexp
       	| rexp MINUS rexp
ExpressionNot(exp)
rexp 			: NOT rexp
ExpressionAnd(exp)
rexp 			: rexp AND rexp
ExpressionOr(exp)
rexp 			: rexp OR rexp
ExpressionCompare(compare, exp1, exp2)
rexp 			: rexp EQUALS rexp
         	| rexp LTE rexp
         	| rexp LT rexp
         	| rexp GTE rexp
         	| rexp GT rexp

ExpressionInvoke(path_exp, attribute_ident, invoke_args)
rexp 			: rexp ACCESS IDENT LPAREN invokearglist RPAREN

ExpressionContructor(ident, invoke_args)
rexp 			: IDENT LPAREN invokearglist RPAREN
					| T_INT LPAREN invokearglist RPAREN
					| T_OBJ LPAREN invokearglist RPAREN
					| T_STRING LPAREN invokearglist RPAREN
					| T_BOOLEAN LPAREN invokearglist RPAREN

[invokearg, invokerag, ...]
invokearglist 	: invokearglist SEP invokearg
            		| invokearg
            		| 

Passthrough
invokearg 			: rexp







Grammer

program 				: classlist statementlist 
classlist 			: classlist class 
								| 
statementlist 	: statementlist statement
                | 
class 				: class_sig class_body 
class_sig 		: CLASS IDENT LPAREN arglist RPAREN
              | CLASS IDENT LPAREN arglist RPAREN EXTENDS IDENT
              | CLASS IDENT LPAREN arglist RPAREN EXTENDS T_OBJ
class_body : LBRACKET statementlist methodlist RBRACKET
arglist 		: arglist SEP arg
            | arg
            | 
arg 		: IDENT TYPE IDENT
        | IDENT TYPE T_INT 
        | IDENT TYPE T_OBJ 
        | IDENT TYPE T_BOOLEAN
        | IDENT TYPE T_STRING 
methodlist : methodlist method
           | 
method  : DEF IDENT LPAREN arglist RPAREN statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE IDENT statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_INT statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_OBJ statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_BOOLEAN statementblock
        | DEF IDENT LPAREN arglist RPAREN TYPE T_STRING statementblock 
statementblock 	: LBRACKET statementlist RBRACKET 
              		| statement_while
              		| statement_return
              		| statement_assign
              		| statement_rexp
statement 				: statement_if
statement_if 			: IF rexp statementblock eliflist
                	| IF rexp statementblock eliflist ELSE statementblock
eliflist 					: ELIF rexp statementblock
statement_while 	: WHILE rexp statementblock
statement_return : RETURN EOS
                 | RETURN rexp EOS
statement_assign : lexp GETS rexp EOS
                 | lexp TYPE IDENT GETS rexp EOS
                 | lexp TYPE T_INT GETS rexp EOS
                 | lexp TYPE T_BOOLEAN GETS rexp EOS
                 | lexp TYPE T_OBJ GETS rexp EOS
                 | lexp TYPE T_STRING GETS rexp EOS
lexp 					: IDENT
       				| rexp ACCESS IDENT
statement_rexp : rexp EOS 
rexp 		: STRING
       	| NUMBER
       	| lexp
rexp 		: MINUS rexp 
rexp 		: LPAREN rexp RPAREN
rexp 		: rexp DIVIDE rexp
       	| rexp TIMES rexp
rexp 		: rexp PLUS rexp
       	| rexp MINUS rexp
rexp 			: NOT rexp
rexp 			: rexp AND rexp
rexp 			: rexp OR rexp
rexp 			: rexp EQUALS rexp
         	| rexp LTE rexp
         	| rexp LT rexp
         	| rexp GTE rexp
         	| rexp GT rexp
rexp 			: rexp ACCESS IDENT LPAREN invokearglist RPAREN
rexp 			: IDENT LPAREN invokearglist RPAREN
					| T_INT LPAREN invokearglist RPAREN
					| T_OBJ LPAREN invokearglist RPAREN
					| T_STRING LPAREN invokearglist RPAREN
					| T_BOOLEAN LPAREN invokearglist RPAREN
invokearglist 	: invokearglist SEP invokearg
            		| invokearg
            		| 
invokearg 			: rexp
