If no value is returned by the action function, the token is simply discarded and the next token read

If your pattern involves whitespace, make sure you use \s. If you need to match the # character, use [#].
