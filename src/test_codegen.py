from hamcrest import *
from testutil import *
from nose.tools import assert_raises
from staticchecker import build_symbol_table
from codegen import *

def test_defineMain():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    gc = define_main(st)

    print(gc)
    assert_that(gc, contains_string("Main->constructor"))


def test_forwardDeclare_SqrDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    gc = forward_declare_class_obj_structs(st)

    print(gc)
    assert_that(gc, contains_string("struct class_Obj_struct"))
    assert_that(gc, contains_string("struct class_Int_struct"))
    assert_that(gc, contains_string("struct class_String_struct"))
    assert_that(gc, contains_string("struct class_Nothing_struct"))
    assert_that(gc, contains_string("struct class_Boolean_struct"))
    assert_that(gc, contains_string("struct class_Pt_struct"))
    assert_that(gc, contains_string("struct class_Rect_struct"))
    assert_that(gc, contains_string("struct class_Main_struct"))
    assert_that(gc, contains_string("struct class_Square_struct"))


def test_methodTableNames_SqrDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    pt = st.root.children[4].class_context
    rect = st.root.children[5].class_context
    square = st.root.children[5].children[0].class_context

    def assert_method_code_name(cls_str, method):
        observed = resolve_method_code_name(method, st)
        expected = "{}_method_{}".format(cls_str, method.name)
        assert_that(expected, is_(observed))

    def assert_inherited_from_obj(method):
        assert_method_code_name("Obj", method)

    def assert_inherited_from_rect(method):
        assert_method_code_name("Rect", method)

    def assert_not_inherited(method):
        assert_method_code_name(method.class_ident, method)

    # Assert inherited from Obj
    assert_inherited_from_obj(pt.find_method("PRINT"))
    assert_inherited_from_obj(rect.find_method("PRINT"))
    assert_inherited_from_obj(square.find_method("PRINT"))
    assert_inherited_from_obj(pt.find_method("EQ"))
    assert_inherited_from_obj(rect.find_method("EQ"))
    assert_inherited_from_obj(square.find_method("EQ"))
    # Assert inherited from Rect
    assert_inherited_from_rect(square.find_method("STR"))
    assert_inherited_from_rect(square.find_method("translate"))
    # Assert not inherited
    assert_not_inherited(pt.find_method("STR"))
    assert_not_inherited(pt.find_method("PLUS"))
    assert_not_inherited(pt.find_method("_x"))
    assert_not_inherited(pt.find_method("_y"))
    assert_not_inherited(rect.find_method("STR"))
    assert_not_inherited(rect.find_method("translate"))


# def get_class_define(cls_name, attributes):
#     ret = code.forward_declare_structs.format(cls=cls_name)
#     attributes_str = ""
#     for a in attributes:
#         attributes_str += code.attribute.format(attr_name=a.name, attr_decl_type=a.declared_type)
#     ret += code.declare_object_struct.format(cls=cls_name, attributes=attributes_str)
#     return ret
#
#
# a1 = symtable.VariableContext("var1", "Bar")
# a2 = symtable.VariableContext("var2", "Baz")
# lst = [a1, a2]
# print(get_class_define("hallo", lst))


# test_defineMain()
# test_forwardDeclare_SqrDeclComplex()