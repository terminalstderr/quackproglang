from hamcrest import *
import test
import qprs
from symtable import *

test_path = test.pos_test_path
test_path2 = test.trials_path


def test_variable_context_rep():
    v = VariableContext("x", "int")

    assert_that(v.declared_type, is_("int"))
    assert_that(str(v), is_("x:int(None)"))


def test_variable_equality_eq1():
    v1 = VariableContext("x", "int")
    v2 = VariableContext("x", "Baz")

    assert_that(v1, is_(v2))


def test_variable_equality_eq2():
    v1 = VariableContext("x", "int")
    v2 = VariableContext("x", "int")

    assert_that(v1, is_(v2))


def test_variable_equality_neq():
    v1 = VariableContext("x", "int")
    v2 = VariableContext("y", "int")

    assert_that(v1, is_not(v2))


def parse_file(fname):
    with open(fname) as f:
        test_data = f.read()

    p = qprs.QuackParser()
    p.build(debug=False)
    ret = p.process(test_data)
    if ret is not 0:
        raise err.QuackError("Unable to parse source file '{}' \nSee stderr for more information".format(fname))
    return p.ast


def get_class_node(program, i=0):
    return program.classes[i]


def get_method_node(class_node, i=0):
    return class_node.body.methods[i]


# Notice that this construction does _NOT_ provide the parent's arguments down to the method...
def test_method_construction1():
    """
    Construct a method from a method AST node... Just
    """
    program = parse_file(test_path + "07_classmethodreturn.qk")
    clazz = get_class_node(program)
    method = get_method_node(clazz)

    mctxt = MethodContext.from_method_node(method, clazz)

    assert_that(mctxt.name, is_("hello"))
    assert_that(mctxt.return_type, is_("Int"))
    assert_that(mctxt.arguments, has_length(0))
    assert_that(mctxt.variable_table, contains(VariableContext("this", clazz.signature.ident)))
    assert_that(mctxt.variable_table, has_length(1))


def init_44():
    program = parse_file(test_path + "44_methodTesting.qk")
    clazz = get_class_node(program)
    method = get_method_node(clazz)
    return method, clazz

def test_build_symboltable():
    program = parse_file(test_path2 + "SqrDecl.qk")

    build_symbol_table(program)

    pass


# def test_method_construction2():
#     """
#     Construct a method from a method AST node... Just
#     """
#     method, clazz = init_44()
#     mctxt = MethodContext(method, clazz)
#
#     assert_that(mctxt.name, is_("hello"))
