# Ryan Leonard 2017
"""
symtable: Quack Symbol Table
A symbol table is a data structure that holds all of the Classes in the program.
Within every class, there is a set of variables (attributes), and a set of methods.
Within every method, there is a set of variables.

A rough hierarchical overview
SymbolTable: [cls1, cls2, cls3, ...]

Class: cls
    attribute_table: [var1, var2, var3, ...]
    method_table: [mthd1, mthd2, ...]
        Method: mthd1
            variable_table: [var1, var2, var3, ...]
"""
import ast
import err
import ivs as givs
import copy


def _symbol_table_with_builtins():
    quackObject = ClassContext(ast.QuackObject)
    root = SymbolTableNode(quackObject)
    sym_table = SymbolTable(root)
    err.assert_insert_success(sym_table.insert_if_parent_exist(ast.QuackInt))
    err.assert_insert_success(sym_table.insert_if_parent_exist(ast.QuackString))
    err.assert_insert_success(sym_table.insert_if_parent_exist(ast.QuackNothing))
    err.assert_insert_success(sym_table.insert_if_parent_exist(ast.QuackBoolean))
    return sym_table


# Builds symbol table from AST
def build_symbol_table(program):
    symbol_table = _symbol_table_with_builtins()

    # Now we have to insert all of the programmer defined classes
    defined_classes = list(program.classes)

    # We have an implicit "MAIN" class which has statements at the top level...
    main = ast.Class(ast.Signature("Main", [], "Obj"), ast.Body(program.statements, []))
    defined_classes.append(main)

    defined_classes_count = len(defined_classes)
    noprogress_i = 0
    while len(defined_classes) > 0:
        cls = defined_classes.pop(0)
        # TODO Check if the class name already exist!
        res = symbol_table.insert_if_parent_exist(cls)
        # If insert failed, append to end of list. Adjust the noprogress counter
        if res is False:
            defined_classes.append(cls)
            noprogress_i += 1
        else:
            noprogress_i = 0
        """
        If we've seen no progress for more than 2*len(allClasses) then we will never see progress
        Example:
            if the unsorted list is worst-scenario of a->b->c->d->...->n, then we will reset when we hit n
            if the unsorted list is worst-scenario with cycle a->b->c->...->n->a, then we will kickout when we hit
            'a' the second time
        hand-wave Proof:
            we will definitely have made a full iteration through unsorted list while having tried to insert
        """
        if noprogress_i >= defined_classes_count:
            unsorted_str = ", ".join([x.signature.ident for x in defined_classes])
            raise err.QuackCheckError("Was unable to establish parent relationship for classes: " + unsorted_str)

    return symbol_table


def get_vars_from_arguments(arguments):
    return [VariableContext(arg.ident, arg.type_ident) for arg in arguments]


class VariableContext:
    def __init__(self, name, declared_type, possibly_initialized=False):
        self.name = name
        self.declared_type = declared_type
        self.inferred_type = None
        self.possibly_not_declared = possibly_initialized

    def __repr__(self):
        return "{}:{}({})".format(self.name, self.declared_type, self.inferred_type)

    def __eq__(self, other):
        return self.name == other.name


class MethodContext:
    """
    Method has four things
    1. a name
    2. a return type
    3. arguments (list of variables)
    4. variables (list of variables, includes arguments, 'this' and local variables initialized in the method)
    """
    def __init__(self):
        self.ast_statements = None
        self.linespan = None
        self.name = None
        self.class_ident = None
        self.return_type = None
        self.arguments = None
        self.variable_table = None
        self.inherited = False

    def find_variable(self, variable_name):
        variable_names = [v.name for v in self.variable_table]
        if variable_name in variable_names:
            index = variable_names.index(variable_name)
            return self.variable_table[index]
        return None

    @staticmethod
    def from_method_node(method_node, class_ident):
        method = MethodContext()
        method.ast_statements = method_node.block.statements
        method.linespan = method_node.linespan
        method.name = method_node.ident
        method.class_ident = class_ident
        method.return_type = method_node.return_type_ident
        method.arguments = get_vars_from_arguments(method_node.args)
        method.variable_table = None
        return method

    @staticmethod
    def constructor(class_node, class_ident):
        method = MethodContext()
        method.ast_statements = class_node.body.statements
        method.linespan = class_node.linespan
        method.name = class_ident
        method.class_ident = class_ident
        method.return_type = class_ident
        method.arguments = get_vars_from_arguments(class_node.signature.arguments)
        method.variable_table = None
        return method

    def construct_variable_table(self):
        # A methods variable table consist of explicit arguments, implicit "this" argument, and local variables
        self.variable_table = list()
        self.variable_table.append(VariableContext("this", self.class_ident))
        self.variable_table += self.arguments
        ivs = givs.get_initialized(self.ast_statements, self.variable_table)
        self.variable_table = ivs.initialized
        self.variable_table += ivs.possibly_initialized

    def __repr__(self):
        return "{}:{} {}".format(self.name, self.return_type, str(self.variable_table))

    def __eq__(self, other):
        return self.name == other.name


class ClassContext:
    def __init__(self, class_node):
        self.class_node = class_node
        self.ident = class_node.signature.ident
        self.method_table = None
        self.method_names = None
        self.constructor = None
        self.attribute_table = None

    def construct_attribute_table(self):
        attributes = givs.get_declared_attributes(self.class_node.body.statements)
        self.attribute_table = attributes

    def construct_constructor(self):
        constructor = MethodContext.constructor(self.class_node, self.ident)
        self.constructor = constructor

    def find_method(self, method_name):
        if method_name in self.method_names:
            index = self.method_names.index(method_name)
            return self.method_table[index]
        return None

    def find_attribute(self, attribute_name):
        attribute_names = [a.name for a in self.attribute_table]
        if attribute_name in attribute_names:
            index = attribute_names.index(attribute_name)
            return self.attribute_table[index]
        return None

    def construct_skeleton_method_table(self, parent_table):
        self.method_table = copy.deepcopy(parent_table)
        for m in self.method_table:
            m.class_ident =  self.ident
            m.inherited = True
        # For every method node in the AST
        overriden = [False for i in range(len(self.method_table))]
        method_names = [m.name for m in self.method_table]
        for method_node in self.class_node.body.methods:
            method_symbolic = MethodContext.from_method_node(method_node, self.ident)
            method_symbolic.inherited = False
            if method_symbolic.name in method_names:
                index = method_names.index(method_symbolic.name)
                if overriden[index]:
                    raise err.QuackCheckError("Duplicate method name '{}' declared.".format(method_node.ident),
                                              method_node.linespan)
                self.method_table[index] = method_symbolic
            else:
                self.method_table.append(method_symbolic)
        self.method_names = [m.name for m in self.method_table]


class SymbolTableNode:
    """
    This takes the responsability of organizing our symbol table.
    Normally, instead of interacting with the node, the user of this node will want to interact directly with the
    class_context... What if they want to know children and parent class though?
    node.class_context
    """
    def __init__(self, class_context):
        self.class_context = class_context
        self.ident = class_context.ident
        self.parent = None
        self.children = list()

    def add_parent(self, parent):
        if self.parent:
            assert False, "Parse Error, Cannot have more than one parent!"
        self.parent = parent

    def add_child(self, child):
        self.children.append(child)

    def get_ancestors(self):
        stnode = self
        ancestors = []
        while stnode:
            ancestors.append(stnode)
            stnode = stnode.parent
        return ancestors

    def lca(self, other):
        """
        Least Common Ancestor (lca) returns the common ancestor of the SymbolTableNode and another SymbolTableNode
        :param other: another SymbolTableNode
        :return: the SymbolTableNode corr
        """
        mine = self.get_ancestors()
        theirs = other.get_ancestors()
        # In the worst case, just return the 'least' ancestor of this class (e.g. Object)
        lca = mine[-1]
        for i in range(1, 1 + min(len(mine), len(theirs))):
            if mine[-i] != theirs[-i]:
                break
            lca = mine[-i]
        return lca

    def is_subclass_of(self, other):
        return self.lca(other).ident == other.ident

    def is_superclass_of(self, other):
        return other.is_subclass_of(self)

    # Takes a class name and returns the corresponding Class
    def find_class_stnode(self, class_ident):
        # Check if this_cls node matches ident
        if self.ident == class_ident:
            return self
        # Recursively check if any child matches ident
        for child in self.children:
            r = child.find_class_stnode(class_ident)
            if r:
                return r
        # If none of the children of this_cls tree had ident, return None
        return None

    # Simply calls a recursive represent method
    def __repr__(self):
        return self.represent_verbose()

    # This will print a pretty version of the tree that is easy to read at command line
    def represent_verbose(self, depth=0):
        ret = "\n" + "  " * depth + self.class_context.ident
        ret += "\n" + "  " * depth + str(self.class_context.method_table)
        ret += "\n" + "  " * depth + str(self.class_context.attribute_table)
        for child in self.children:
            ret += child.represent_verbose(depth + 1)
        return ret

    # This will print a pretty version of the tree that is easy to read at command line
    def represent(self, depth=0):
        ret = "\n" + "  " * depth + self.class_context.ident
        for child in self.children:
            ret += child.represent(depth + 1)
        return ret


class SymbolTable:
    def __init__(self, root):
        self.root = root
        self.x = 0

    def as_list(self):
        ret = [self.root]
        ret += self._as_list(self.root)
        return ret

    def _as_list(self, node):
        ret = list(node.children)
        for child in node.children:
            ret += self._as_list(child)
        return ret

    # def least_common_ancestor(self, cls_id1, cls_id2):
    #     cls1 = self.find_class(cls_id1)
    #     if not cls1:
    #         raise err.QuackInternalError("Could not find class {} when attempting to determine LCA".format(cls_id1))
    #     cls2 = self.find_class(cls_id2)
    #     if not cls2:
    #         raise err.QuackInternalError("Could not find class {} when attempting to determine LCA".format(cls_id2))
    #     return cls1.find_common_ancestor(cls2)

    # def lca(self, cls_id1, cls_id2):
    #     return self.least_common_ancestor(cls_id1, cls_id2)

    def is_inference_complete(self):
        """
        changed = true
        while(changed):
            changed = false
            for assn_statement in statements:
                var = find_variable(assn_statement.location.ident)
                old = var.inferred_type
                new = typeof(assn_statement)
                if new != old:
                    var.inferred_type = new
                    changed = true
        """
        self.x += 1
        if self.x > 20:
            return True
        return False

    # This insert method will only insert the class if the the parent exist
    # somewhere within the tree, otherwise the insert will fail.
    def insert_if_parent_exist(self, cls):
        err.assert_node_type(cls, ast.Class)
        parent = self.find_class_stnode(cls.signature.parent_ident)
        if parent:
            child_cls = ClassContext(cls)
            child_nd = SymbolTableNode(child_cls)
            parent.add_child(child_nd)
            child_nd.add_parent(parent)
            return True
        else:
            return False

    # Notice that "find_class" is the only recursive call, all other calls are based on iterative structures instead of
    # recursive structures.
    def find_class_stnode(self, class_ident):
        return self.root.find_class_stnode(class_ident)

    def find_class(self, class_ident):
        cls_stnode = self.find_class_stnode(class_ident)
        if cls_stnode:
            return cls_stnode.class_context
        return None

    def __repr__(self):
        return self.root.__repr__()

    # # Used to get the type of an expression
    # # Will return a reference to the class node associated to
    # def typeof(self, expression):
    #     """
    #     typeof will either return an ident, return None, or raise an exception.
    #    typeof will try to infer the type of the expression, but if it is not possible with the given information, None
    #    will be returned. If this is the case, simply rerun typeof on this expression after running typeof on all other
    #     expressions.
    #     An exception will be raised if it is passed anything but an expression, or if an unknown constructor is used
    #     :param expression:
    #     :return:
    #     """
    #     # Every expression caches its type once it is found
    #     if not issubclass(type(expression), ast.Expression):
    #         raise err.QuackInternalError(
    #             "Can only determine typeof expressions, cannot infer typeof '{}'".format(expression))
    #
    #     # If we already know the type of this expression, then we are already done!
    #     if expression.etype:
    #         return expression.etype
    #
    #     # If we don't already know the type of this expression, then we try our best to infer based on
    #     # * Method Return types,
    #     # * Constructors used,
    #     # * Operation types,
    #     # *
    #     # ExpressionConstructor -- LOOKUP CONS: if there is a constructor in the expression, this is easiest since we
    #     # just return the type corresponding to that constructor.
    #     ret_type = None
    #     if type(expression) is ast.ExpressionConstructor:
    #         cls = self.find_class_stnode(expression.ident)
    #         if not cls:
    #             raise err.QuackInternalError("Could not find constructor for class {}".format(expression.ident))
    #         ret_type = cls.ident
    #
    #     # ExpressionInvoke -- LOOKUP METH: if there is an invokation, then this is also easy since we can just assume
    #     # the return type...
    #     elif type(expression) is ast.ExpressionInvoke:
    #         method = self.find_method(expression)
    #         if method:
    #             ret_type = method.return_type
    #
    #     # Expression -- LOOKUP VAR: if the expression is just a value, look it up in the symbol table and get its
    #     # inferred type
    #     elif type(expression) is ast.ExpressionBare:
    #         # Need to find
    #         var = self.find_variable(expression.value)
    #         if var:
    #             if var.inferred_type:
    #                 ret_type = var.inferred_type
    #
    #     # ExpressionBinop --  REC if the expression is a binop, get the type of both subexpressions, ensure they are
    #     # equal, then return that type
    #     elif type(expression) is ast.ExpressionBinop:
    #         type_exp1 = self.typeof(expression.exp1)
    #         type_exp2 = self.typeof(expression.exp2)
    #         err.assert_same(type_exp1, type_exp2)
    #         ret_type = type_exp1
    #
    #     # ExpressionUnop -- REC if the expression is a unaop, get the type of the subexpression and return that
    #     elif type(expression) is ast.ExpressionUnaop:
    #         ret_type = self.typeof(expression.exp1)
    #
    #     # ExpressionBoolean -- simply return boolean as the inferred type
    #     elif type(expression) is ast.ExpressionNot or type(expression) is ast.ExpressionAnd or type(
    #             expression) is ast.ExpressionOr or type(expression) is ast.ExpressionCompare:
    #         ret_type = ast.QuackBoolean.signature.ident
    #
    #     else:
    #         raise err.QuackInternalError("Unhandled expression type check!")
    #
    #     expression.etype = ret_type
    #     return expression.etype

    # TODO The below two methods dont make any sense from a 'symbol table' perspective...
    # def find_variable(self, variable_location):
    #     """
    #     Very similar to find_method
    #     Give a variable location (e.g. an ast.Location) and get back a chk.Variable structure.
    #     Will return None if the variable's class cannot be inferred, in which case retry after another typecheck pass.
    #     Will raise exception if the variable's class is known but the variable is not found in the class.
    #     """
    #     path = variable_location.path_exp
    #     name = variable_location.ident
    #     if path:
    #         cls = self.typeof(path)
    #         cls_node = self.find_class(cls)
    #     else:
    #         cls_node = self.find_class("Main")
    #     if not cls_node:
    #         return None
    #     attributes = [attr for attr in cls_node.attribute_table if attr.name == name]
    #     if len(attributes) != 1:
    #         if name.lower() in lex.QuackLexer.keywords.keys():
    #             msg = "Was unable to find variable '{}' in symbol table of class '{}', maybe you meant '{}'?".format(
    #                 name, cls_node.ident, name.lower())
    #             raise err.QuackInternalError(msg, variable_location.linespan)
    #         if len(attributes) > 1:
    #             msg = "Found multiple copies of variable named '{}' in symbol table of class '{}'".format(
    #                 name, cls_node.ident)
    #             raise err.QuackInternalError(msg, variable_location.linespan)
    #         raise err.QuackInternalError(
    #             "Was unable to find variable '{}' in symbol table of class '{}'".format(name, cls_node.ident),
    #             variable_location.linespan)
    #
    #     return attributes[0]
    #
    # def find_method(self, method_invoke):
    #     """
    #     Very similar to find_variable
    #     Give a method invocation (e.g. an ast.ExpressionInvoke) and get back a chk.Method structure.
    #     Will return None if the method's class cannot be inferred, in which case retry after another typecheck pass.
    #     Will raise an exception if the method's class is known but the method is not found in the class.
    #     """
    #     path = method_invoke.path_exp
    #     name = method_invoke.method_ident
    #     if path:
    #         cls = self.typeof(path)
    #         cls_node = self.find_class(cls)
    #     else:
    #         cls_node = self.find_class("Main")
    #     if not cls_node:
    #         return None
    #     methods = [mthd for mthd in cls_node.method_table if mthd.name == name]
    #     if len(methods) != 1:
    #         raise err.QuackInternalError(
    #             "Was unable to find method '{}' in symbol table of class '{}'".format(name, cls_node.ident),
    #             method_invoke.linespan)
    #     return methods[0]
