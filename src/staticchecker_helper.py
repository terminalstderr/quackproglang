import ast
import err


def find_variable(ivs, varname):
    variable_names = [var.name for var in ivs]
    if varname not in variable_names:
        return None
    i = variable_names.index(varname)
    return ivs[i]


def find_variable_throw(ivs, varname, linespan):
    ret = find_variable(ivs, varname)
    if not ret:
        # "Maybe you meant 'this.varname'?"
        raise err.QuackCheckError("Was unable to resolve variable {}".format(varname), linespan)
    return ret


def find_class_throw(st, class_name, linespan):
    ret = st.find_class(class_name)
    if not ret:
        raise err.QuackCheckError("Unable to find class '{}'".format(class_name), linespan)
    return ret


def find_method_throw(cls, method_name, linespan):
    ret = cls.find_method(method_name)
    if not ret:
        raise err.QuackCheckError("Unable to find method '{}' in class '{}'".format(method_name, cls.ident), linespan)
    return ret


def find_attribute_throw(cls, attribute_name, linespan):
    ret = cls.find_attribute(attribute_name)
    if not ret:
        raise err.QuackCheckError("Unable to find attribute '{}' in class '{}'".format(attribute_name, cls.ident), linespan)
    return ret


def typeof(e, st, ivs):
    err = None
    ret = e.etype

    if e.etype:
        return e.etype

    if type(e) is ast.ExpressionInvoke:
        t = typeof(e.path_exp, st, ivs)
        c = find_class_throw(st, t, e.linespan)
        m = find_method_throw(c, e.method_ident, e.linespan)
        ret = m.return_type

    elif type(e) is ast.ExpressionBare:
        # Right here we are assuming that any bare expression contains a location...
        if not e.value.path_exp:
            v = find_variable_throw(ivs, e.value.ident, e.linespan)
            ret = v.inferred_type
        else:
            t = typeof(e.value.path_exp, st, ivs)
            c = find_class_throw(st, t, e.linespan)
            v = find_attribute_throw(c, e.value.ident, e.linespan)
            ret = v.inferred_type

    elif type(e) is ast.ExpressionConstructor:
        ret = e.ident

    elif type(e) is ast.ExpressionBinop:
        # Assume that the return type of a Binop is the same as the two sub expressions
        ret = typeof(e.exp1, st, ivs)

    elif type(e) is ast.ExpressionUnaop:
        # Assume that the return type of a Binop is the same as the two sub expressions
        ret = typeof(e.exp1, st, ivs)

    elif type(e) in [ast.ExpressionNot, ast.ExpressionOr, ast.ExpressionAnd, ast.ExpressionCompare]:
        ret = ast.QuackBoolean.signature.ident

    e.etype = ret
    return e.etype


class Checker:
    def __init__(self):
        self.checks = list()
        self.node_types = list()
        self.context = None

    @staticmethod
    def init_single(check, node_types):
        c = Checker()
        c.add_check(check)
        c.add_node_types(node_types)
        return c

    def check_bottom_up(self, node):
        # Depth first
        for child in node.children():
            self.check_bottom_up(child)
        if type(node) in self.node_types:
            for check in self.checks:
                check(node, self.context)

    def add_check(self, check):
        self.checks.append(check)

    def add_node_types(self, node_types):
        self.node_types += node_types

    def run(self, smnt_list, context):
        self.context = context
        for smnt in smnt_list:
            self.check_bottom_up(smnt)
