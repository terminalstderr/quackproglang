#!/usr/bin/python3
import err
from staticchecker import build_symbol_table
from staticchecker import run_checks
from codegen import generate_code
from utility import *
from testutil import parse_file
from sys import exit

def main():
    try:
        submain(sys.argv[1], sys.argv[2])
    except err.QuackError as e:
        eprint(e.pretty())
        exit(1) 


def submain(fname, outfname):
    ast_program = parse_file(fname)
    symbol_table = build_symbol_table(ast_program)
    run_checks(symbol_table)
    generate_code(symbol_table, outfname)

if __name__ == "__main__":
    main()
