from ast import *
import ivs
from hamcrest import *


def test_getExpressionUsedVariables_andExpression():
    e = ExpressionAnd(ExpressionBare(Location("x")), ExpressionBare(Location("y")))

    g = ivs.GetUsedVariables()
    used = g.get_expression_used(e)
    assert_that(used[0].ident, is_("x"))
    assert_that(used[1].ident, is_("y"))


def test_getExpressionUsedVariables_andExpression():
    e = ExpressionAnd(
        ExpressionInvoke(ExpressionBare(Location("x")), "foo", []),
        ExpressionBare(Location("y")))

    g = ivs.GetUsedVariables()
    used = g.get_expression_used(e)
    assert_that(used[0].ident, is_("x"))
    assert_that(used[1].ident, is_("y"))



def test_getExpressionUsedVariables_andExpression():
    e = ExpressionAnd(
        ExpressionInvoke(ExpressionBare(Location("x")), "foo", []),
        ExpressionBare(Location("y", ExpressionBare(Location("this"))))
    )

    g = ivs.GetUsedVariables()
    used = g.get_expression_used(e)
    assert_that(used[0].ident, is_("x"))
    assert_that(used[1].ident, is_("y"))


