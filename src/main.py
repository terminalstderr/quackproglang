#!/usr/bin/python3
import err
import chk
from symtable import SymbolTable
from qprs import QuackParser
from utility import *

def main():
    try:
        submain(sys.argv[1])
    except err.QuackError as e:
        eprint(e.pretty())
        raise e


def submain(fname):
    with open(fname) as f:
        test_data = f.read()

    p = QuackParser()
    p.build(debug=False)
    parseRet = p.process(test_data)

    if parseRet is not 0:
        raise err.QuackError("Unable to parse source file '{}' \nSee stderr for more information".format(fname))

    print(p.ast)
    chk.symbol_table = SymbolTable()
    chk.symbol_table.build(p.ast)

    # Check for valid constructor usage, check for decleration before usage
    checker = chk.QuackChecker()
    checker.add_check(chk.check_valid_constructor)
    ret = checker.run(p.ast)
    if ret:
        raise err.QuackError("ERROR: {}".format(ret))

    # Run typechecker
    # typechecker = chk.QuackChecker()
    # typechecker.add_check(chk.check_type_single_pass)
    # # each pass will update the global symbol_table
    # while not chk.symbol_table.is_inference_complete():
    #     # When determining
    #     ret = typechecker.run(p.ast)
    #     if ret:
    #         raise err.QuackError("ERROR: {}".format(ret))

    checker = chk.QuackChecker()
    checker.add_check(chk.check_conditional_expressions)
    checker.add_check(chk.check_return_statements)
    ret = checker.run(p.ast)
    if ret:
        raise err.QuackError("ERROR: {}".format(ret))


if __name__ == "__main__":
    main()
