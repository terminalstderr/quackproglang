c_includes = """
// Code generated from Quack
// 2017 - Ryan Leonard
#include <stdio.h>
#include <stdlib.h>
#include "Builtins.h"
"""

forward_declare_structs = """
// Forward deceleration of structs for class {cls}
struct class_{cls}_struct;
typedef struct class_{cls}_struct* class_{cls};
struct class_{cls}_struct the_class_{cls}_struct;
extern class_{cls} the_class_{cls};

struct obj_{cls}_struct;
typedef struct obj_{cls}_struct* obj_{cls};
"""


# attr_decl_type
# EX: a1 = attribute.format(attr_name="v1", attr_decl_type="Int")
# EX: a2 = attribute.format(attr_name="v2", attr_decl_type="Obj")
attribute = """
    obj_{attr_decl_type} {attr_name};"""


# cls = class name
# EX: dca = declare_object_struct.format(cls="X", attributes=a1 + a2)
declare_object_struct = """
// Deceleration of obj_{cls}
struct obj_{cls}_struct
{{
    class_{cls} clazz;
{attributes}
}};
"""


# Build a comma seperated string from a list
def css(lst):
    ret = ", ".join(lst)
    return ret


# type
method_declaration_argument= """obj_{type}"""


# method_declaration
# args =
# EX: md = method_declaration.format(method_name="PLUS", ret_type="Int", args=
method_declaration = """
    obj_{ret_type} (*{method_name}) ({args});"""


declare_class_struct = """
// Deceleration of class_{cls}_struct
struct class_{cls}_struct
{{
{methods}
}};
"""


define_main = """
int main(int argc, char** argv)
{
  the_class_Main->constructor();
  printf("\\n--- Terminated successfully (woot!) ---\\n");
  exit(0);
}
"""


# name the variable name
# type the type of the argument
argument = """obj_{type} {name}"""


# args the constructor arguments
# cls class_ident
# statements the statement code generated
constructor_define = """
obj_{cls} new_{cls}({args})
{{
    obj_{cls} this = (obj_{cls}) malloc(sizeof(struct obj_{cls}_struct));
    this->clazz = the_class_{cls};
"""
constructor_return = """
    return this;
}
"""

method_name = """{cls}_method_{method_name}"""


# ret_type
# code_method_name
# args
# statements
method = """
obj_{ret_type} {code_method_name}({args})
"""


# cls
define_the_class = """
/* The {cls} Class (a singleton) */
struct  class_{cls}_struct  the_class_{cls}_struct = {{
    new_{cls},     /* Constructor */
    {method_names}
}};

class_{cls} the_class_{cls} = &the_class_{cls}_struct;
"""

allocation="""    obj_{type} {name};
"""
