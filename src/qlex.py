"""
qlex: Quack Lexer
"""
import ply.lex as lex
from utility import *


class QuackLexer:
    def __init__(self):
        self.lexer = None
        self.errors = list()
        self.verbose = False

    def _log(self, msg):
        if self.verbose:
            print(msg, end="")

    def error(self, msg):
        self.errors += msg
        eprint(msg)

    keywords = {
        "class": "CLASS",
        "def": "DEF",
        "extends": "EXTENDS",
        "if": "IF",
        "elif": "ELIF",
        "else": "ELSE",
        "while": "WHILE",
        "return": "RETURN",
        "String": "T_STRING",
        "Int": "T_INT",
        "Obj": "T_OBJ",
        "Boolean": "T_BOOLEAN",
        "true": "TRUE",
        "false": "FALSE",
        "and": "AND",
        "or": "OR",
        "not": "NOT",
        # UNUSED "Nothing" : "NOTHING",
        # UNUSED "none" : "NONE"
    }
    punctuation = [
        "PLUS",
        "MINUS",
        "TIMES",
        "DIVIDE",
        "LPAREN",
        "RPAREN",
        "LBRACKET",
        "RBRACKET",
        "GETS",
        "SEP",
        "EOS",
        "ACCESS",
        "TYPE",
        "EQUALS",
        "LTE",
        "LT",
        "GTE",
        "GT",
    ]

    tokens = ["IDENT",
              "NUMBER",
              "STRING",
              # "DUMMY",
              # UNUSED "L3STRING",
              # UNUSED "R3STRING",
              # UNUSED "LSTRING",
              # UNUSED "ESCAPE",
              # UNUSED "RSTRING",
              # UNUSED "COMMENT",
              # UNUSED "RBCOMMENT",
              # UNUSED "LBCOMMENT"
              ] + punctuation + list(keywords.values())

    states = (('comment', 'exclusive'),
              ('string', 'exclusive'),
              ('3string', 'exclusive'),)

    # t_DUMMY=r'[a-zA-Z]+'

    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_TIMES = r'\*'
    t_DIVIDE = r'/'
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRACKET = r'{'
    t_RBRACKET = r'}'
    t_GETS = r'='
    t_SEP = r','
    t_EOS = r';'
    t_ACCESS = r'\.'
    t_TYPE = r':'
    t_EQUALS = r'=='
    t_LTE = r'<='
    t_LT = r'<'
    t_GTE = r'>='
    t_GT = r'>'

    def t_NUMBER(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    def t_COMMENT(self, t):
        r'//.*'
        self._log("Oneline comment\n")
        pass

    def t_LBCOMMENT(self, t):
        r'/[*]'
        t.lexer.code_start = t.lexer.lexpos
        t.lexer.push_state('comment')
        self._log("Starting block comment\n")
        pass

    def t_comment_error(self, t):
        self.error("COMMENT: Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

    t_comment_ignore = ''

    def t_comment_RBCOMMENT(self, t):
        r'[*]/'
        t.lexer.pop_state()
        self._log("\nEnding block comment\n")
        pass

    def t_comment_COMMENT(self, t):
        r'(.|\n)'
        if t.value is '\n':
            self._log("(absorbed newline)")
            t.lexer.lineno += 1
        self._log(".")
        pass

    # Handling """Anything Goes!""" maddness
    _3string = ""

    def t_L3STRING(self, t):
        r'["]{3}'
        t.lexer.push_state('3string')
        self._3string = ""
        self._log("Starting String\n")
        pass

    def t_3string_R3STRING(self, t):
        r'["]{3}'
        t.lexer.pop_state()
        self._log("\nString Complete\n")
        t.type = "STRING"
        t.value = self._3string
        return t

    def t_3string_STRING(self, t):
        r'(.|\n)'
        if t.value is '\n':
            t.lexer.lineno += 1
            self._log("(absorbed newline)")
        self._log(t.value)
        self._3string += t.value

    t_3string_ignore = ''

    def t_3string_error(self, t):
        self.error("3STRING: Illegal character??? '%s'" % t.value[0])
        t.lexer.skip(1)

    # Handling "Simple String" joy
    _string = ""
    _broke = False

    def t_LSTRING(self, t):
        r'["]'
        t.lexer.push_state('string')
        self._string = ""
        self._broke = False
        self._log("Starting String\n")
        pass

    def t_string_RSTRING(self, t):
        r'["]'
        t.lexer.pop_state()
        self._log("\nString Complete\n")
        # XXX Should this be T_STRING or STRING?
        t.type = "STRING"
        t.value = self._string
        if self._broke:
            self.error(r"""{}: Illegal escape code; only \\, \0, \t, \n, \r, \n are permitted (at '{}')""".format(
                t.lexer.lineno, self._string))
            return
        return t

    def t_string_ESCAPE(self, t):
        r'[\\].'
        self._log(t.value)
        if t.value[1] in "0btnrf\"\\":
            self._log(t.value)
            self._string += t.value
        else:
            self._broke = True
            self._string += t.value
            return

    def t_string_STRING(self, t):
        r'.'
        self._log(t.value)
        self._string += t.value

    t_string_ignore = ''

    def t_string_error(self, t):
        if t.value[0] == '\n':
            self.error("{}: Unclosed string?	Encountered newline in quoted string. (at '{}\n)".format(
                t.lexer.lineno, self._string))
            t.lexer.skip(1)
            t.lexer.lineno += 1
            t.lexer.pop_state()
            return
        else:
            self.error("{}: String Parse Error! (at '{})".format(t.lexer.lineno, self._string))

    def t_IDENT(self, t):
        r'[a-zA-Z_][a-zA-Z_0-9]*'
        t.type = self.keywords.get(t.value, 'IDENT')
        return t

    # Define a rule so we can track line numbers
    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    t_ignore = ' \t'

    def t_error(self, t):
        if t.value[0] == ' ':
            self._log("This is not a true error, we are unsure what is happening here...\n")
            t.lexer.skip(1)
            return
        self.error("INIT: Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

    def build(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    def process(self, data):
        self.lexer.input(data)
        return self.lexer
