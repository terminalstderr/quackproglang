import err as err
from glob import glob
import qprs

atwork = False
laptop = True

# Workspace
if atwork:
    pos_test_path = "/home/ryan/Documents/quackproglang/test_pos/"
    neg_test_path = "/home/ryan/Documents/quackproglang/test_neg/"
    trials_path = "/home/ryan/Documents/quackproglang/trial/samples/"
# Laptop
if laptop:
    pos_test_path = "C:/Users/ryanl/repos/quackproglang/test_pos/"
    neg_test_path = "C:/Users/ryanl/repos/quackproglang/test_neg/"
    trials_path = "C:/Users/ryanl/repos/quackproglang/trial/samples/"
# Desktop
if not laptop and not atwork:
    pos_test_path = "C:/Users/Ryan/repos/quackproglang/test_pos/"
    neg_test_path = "C:/Users/Ryan/repos/quackproglang/test_neg/"
    trials_path = "C:/Users/Ryan/repos/quackproglang/trial/samples/"


pos_files = glob(pos_test_path + "/*.qk")
neg_files = glob(neg_test_path + "/*.qk")


def parse_file(fname):
    with open(fname) as f:
        test_data = f.read()

    p = qprs.QuackParser()
    p.build(debug=False)
    ret = p.process(test_data)
    if ret is not 0:
        raise err.QuackError("Unable to parse source file '{}' \nSee stderr for more information".format(fname))
    return p.ast



