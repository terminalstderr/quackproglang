"""
ast: Abstract Syntax Tree
"""
# Ryan Leonard 2017
from enum import Enum

quack_obj_ident = "Obj"
quack_nothing_ident = "Nothing"
quack_int_ident = "Int"
quack_bool_ident = "Boolean"
quack_str_ident = "String"


class Unaops(Enum):
    NEG = 1


class Binops(Enum):
    MUL = 1
    DIV = 2
    PLU = 3
    SUB = 4


class Compares(Enum):
    LE = 1
    LT = 2
    GT = 3
    GE = 4
    EQ = 5


# Class is a node, children are Body and
# 
# Make every node have a 'children' method that returns all child nodes...
# Idea is to have any production based object be put into a list in the return from the children call
class Node:
    linespan = (-1, -1)

    def children(self):
        raise NotImplementedError("TBD")


class Program(Node):
    def children(self):
        return self.classes + self.statements

    def __init__(self, classes, statements):
        self.classes = classes
        self.statements = statements

    def __repr__(self):
        return "Program: \nCLASSES\n{}\nENDCLASSES\nSTATEMENTS\n{}".format(nlstr(self.classes), nlstr(self.statements))


class Class(Node):
    def children(self):
        return [self.body, self.signature]

    def __init__(self, signature, body):
        self.signature = signature
        self.body = body

    def __repr__(self):
        return "class {}\n{{\n{}\n}}".format(self.signature, self.body)


class Signature(Node):
    def children(self):
        return self.arguments

    def __init__(self, ident, arguments, parent_ident):
        self.ident = ident
        self.arguments = arguments
        self.parent_ident = parent_ident

    def __repr__(self):
        return "{}({}):{}".format(self.ident, lstr(self.arguments), self.parent_ident)


class Body(Node):
    def children(self):
        return self.statements + self.methods

    def __init__(self, statements, methods):
        self.statements = statements
        self.methods = methods

    def __repr__(self):
        return "{}{}".format(nlstr(self.statements, 2), nlstr(self.methods, 2))


def nlstr(lst, shiftwidth=0):
    return " " * shiftwidth + "\n".join(str(e) for e in lst)


class Argument(Node):
    def children(self):
        return []

    def __init__(self, ident, type_ident):
        self.ident = ident
        self.type_ident = type_ident

    def __repr__(self):
        return "{}: {}".format(self.ident, self.type_ident)


class Method(Node):
    def children(self):
        return [self.block]

    def __init__(self, ident, args, block, return_type_ident=quack_nothing_ident):
        self.ident = ident
        self.args = args
        self.block = block
        self.return_type_ident = return_type_ident

    def __repr__(self):
        return "{}({}):{} {}".format(self.ident, lstr(self.args), self.return_type_ident, self.block)


# Passthrough...
class Block(Node):
    def children(self):
        return self.statements

    def __init__(self, statements):
        self.statements = statements

    def __repr__(self):
        return "\n  {{\n{}\n  }}".format(nlstr(self.statements, 4))


# Statement is added just to give us a hierarchy of statements
class Statement(Node):
    def children(self):
        return []


class StatementIf(Statement):
    def children(self):
        return [self.block] + self.eliflist + ([self.else_block] if self.else_block else [])

    def __init__(self, condition_exp, block, eliflist, else_block=None):
        self.condition_exp = condition_exp
        self.block = block
        self.eliflist = eliflist
        self.else_block = else_block

    def __repr__(self):
        return "if ({}){}\n{}\nelse {}".format(self.condition_exp, self.block, nlstr(self.eliflist, 4), self.else_block)


class SubStatementElif(Statement):
    def children(self):
        return [self.condition_exp, self.block]

    def __init__(self, condition_exp, block):
        self.condition_exp = condition_exp
        self.block = block

    def __repr__(self):
        return "elif({}){}".format(self.condition_exp, self.block)


class StatementWhile(Statement):
    def children(self):
        return [self.condition_exp, self.block]

    def __init__(self, condition_exp, block):
        self.condition_exp = condition_exp
        self.block = block

    def __repr__(self):
        return "while({}){}".format(self.condition_exp, self.block)


class StatementRet(Statement):
    def children(self):
        return [self.return_exp] if self.return_exp else []

    def __init__(self, return_exp=None):
        self.return_exp = return_exp

    def __repr__(self):
        return "return {};".format(self.return_exp)


# TODO Default type should probably be obj
class StatementAssign(Statement):
    def children(self):
        return [self.location, self.expression]

    def __init__(self, location, expression, type_ident="unknown"):
        self.location = location
        self.expression = expression
        self.type_ident = type_ident

    def __repr__(self):
        return "{}:{} = {}".format(self.location, self.type_ident, self.expression)


class Location(Node):
    def children(self):
        return [self.path_exp] if self.path_exp else []

    def __init__(self, ident, path_exp=None):
        self.ident = ident
        self.path_exp = path_exp

    def __repr__(self):
        return "{}.{}".format(self.path_exp, self.ident)

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()  # TODO and self.path_exp == other.path_exp

    def __hash__(self):
        return hash(self.ident)  # TODO and self.path_exp == other.path_exp


class StatementExp(Node):
    def children(self):
        return [self.exp]

    def __init__(self, exp):
        self.exp = exp

    def __repr__(self):
        return "StatementExp: {}".format(self.exp)


class Expression(Node):
    def __init__(self):
        self.etype = None

    def children(self):
        return []


class ExpressionBare(Expression):
    def children(self):
        return [self.value] if type(self.value) is Location else []

    def __init__(self, value, etype=None):
        Expression.__init__(self)
        self.value = value
        self.etype = etype

    def __repr__(self):
        return "{}".format(self.value)


class ExpressionUnaop(Expression):
    def children(self):
        return [self.exp1]

    def __init__(self, operation, exp1):
        Expression.__init__(self)
        self.operation = operation
        self.exp1 = exp1

    def __repr__(self):
        return "{}({})".format(self.operation.name, self.exp1)


class ExpressionBinop(Expression):
    def children(self):
        return [self.exp1, self.exp2]

    def __init__(self, operation, exp1, exp2):
        Expression.__init__(self)
        self.operation = operation
        self.exp1 = exp1
        self.exp2 = exp2

    def __repr__(self):
        return "{}({},{})".format(self.operation.name, self.exp1, self.exp2)


class ExpressionNot(Expression):
    def children(self):
        return [self.exp]

    def __init__(self, exp):
        Expression.__init__(self)
        self.exp = exp

    def __repr__(self):
        return "NOT({})".format(self.exp)


class ExpressionAnd(Expression):
    def children(self):
        return [self.exp1, self.exp2]

    def __init__(self, exp1, exp2):
        Expression.__init__(self)
        self.exp1 = exp1
        self.exp2 = exp2

    def __repr__(self):
        return "AND({},{})".format(self.exp1, self.exp2)


class ExpressionOr(Expression):
    def children(self):
        return [self.exp1, self.exp2]

    def __init__(self, exp1, exp2):
        Expression.__init__(self)
        self.exp1 = exp1
        self.exp2 = exp2

    def __repr__(self):
        return "OR({},{})".format(self.exp1, self.exp2)


class ExpressionCompare(Expression):
    def children(self):
        return [self.exp1, self.exp2]

    def __init__(self, compare, exp1, exp2):
        Expression.__init__(self)
        self.compare = compare
        self.exp1 = exp1
        self.exp2 = exp2

    def __repr__(self):
        return "{} {} {}".format(self.exp1, self.compare.name, self.exp2)


# TODO Isn't it true for invoke and constructor that invoke arguments could be arbitrary expressions?
class ExpressionInvoke(Expression):
    def children(self):
        ret = list(self.invoke_args)
        ret.append(self.path_exp)
        return ret

    def __init__(self, path_exp, method_ident, invoke_args):
        Expression.__init__(self)
        self.path_exp = path_exp
        self.method_ident = method_ident
        self.invoke_args = invoke_args

    def __repr__(self):
        return "{}.{}({})".format(self.path_exp, self.method_ident, lstr(self.invoke_args))


class ExpressionConstructor(Expression):
    def children(self):
        return list(self.invoke_args)

    def __init__(self, ident, invoke_args):
        Expression.__init__(self)
        self.ident = ident
        self.invoke_args = invoke_args

    def __repr__(self):
        return "{}({})".format(self.ident, lstr(self.invoke_args))


def lstr(lst):
    return ", ".join(str(e) for e in lst)

# Notice that our "QuackMain" object is actually instantiated within the checker
# These are simply stubs for typechecking
# TODO give them builtin methods.
eblock=Block([])
objarg = Argument("o", quack_obj_ident)
QuackObject = Class(
    Signature(quack_obj_ident, [], None),
    Body([], [
        Method("STR", [], eblock, quack_str_ident),
        Method("PRINT", [], eblock, quack_nothing_ident),
        Method("EQ", [objarg], eblock, quack_bool_ident),
    ])
)

QuackNothing = Class(
    Signature(quack_nothing_ident, [], quack_obj_ident),
    Body([], [])
)

QuackBoolean = Class(
    Signature(quack_bool_ident, [], quack_obj_ident),
    Body([], [])
)

intarg = Argument("n", quack_int_ident)
QuackInt = Class(
    Signature("Int", [], quack_obj_ident),
    Body([], [
        Method("PLUS", [intarg], eblock, quack_int_ident),
        Method("MINUS", [intarg], eblock, quack_int_ident),
        Method("TIMES", [intarg], eblock, quack_int_ident),
        Method("DIVIDE", [intarg], eblock, quack_int_ident),
        Method("EQUALS", [intarg], eblock, quack_bool_ident),
        Method("ATMOST", [intarg], eblock, quack_bool_ident),
        Method("LESS", [intarg], eblock, quack_bool_ident),
        Method("ATLEAST", [intarg], eblock, quack_bool_ident),
        Method("MORE", [intarg], eblock, quack_bool_ident),
    ])
)

strarg = Argument("s", quack_str_ident)
QuackString = Class(
    Signature(quack_str_ident, [], quack_obj_ident),
    Body([], [
        Method("LESS", [strarg], eblock, quack_bool_ident),
        Method("PLUS", [strarg], eblock, quack_str_ident),
    ])
)
