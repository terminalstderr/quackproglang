import err


# Assumption: The number of arguments must be the same in both methods
# Note: For each of the following method think of '1' as 'original' and '2' as 'overrider'

def typecheck_methodoverride(m1, m2, sym_table):
    errmsg = typecheck_methodoverride_return(m1, m2, sym_table)
    if errmsg:
        raise err.QuackCheckError(errmsg, (m2.linespan[0], m2.linespan[0]))
    errmsg = typecheck_methodoverride_args(m1, m2, sym_table)
    if errmsg:
        raise err.QuackCheckError(errmsg, (m2.linespan[0], m2.linespan[0]))


def typecheck_methodoverride_return(m1, m2, sym_table):
    cls1 = sym_table.find_class_stnode(m1.return_type)
    cls2 = sym_table.find_class_stnode(m2.return_type)
    if not cls2.is_subclass_of(cls1):
        msg = "Method override with non-covariant return type\n {}(...):{} must be subtype of {}(...):{}" \
              "".format(m2.name, m2.return_type, m1.name, m1.return_type)
        return error_message(m1, m2, msg)


def typecheck_methodoverride_args(m1, m2, sym_table):
    if len(m1.arguments) != len(m2.arguments):
        return error_message(m1, m2, "Method override with different number of arguments")

    for arg1, arg2 in zip(m1.arguments, m2.arguments):
        cls1 = sym_table.find_class_stnode(arg1.declared_type)
        cls2 = sym_table.find_class_stnode(arg2.declared_type)
        if not cls2.is_superclass_of(cls1):
            msg = "Method override with non-contravariant argument type\n {}:{} must be supertype of {}:{}" \
                  "".format(arg2.name, arg2.declared_type, arg1.name, arg1.declared_type)
            return error_message(m1, m2, msg)


def error_message(m1, m2, msg):
    return "{}::{}:{} overrides method {}::{}:{} \n{}" \
           "".format(m2.class_ident, m2.name, m2.linespan[0], m1.class_ident, m1.name, m1.linespan[0], msg)
