import symtable
import codegen_templates as code
import ast
from staticchecker_helper import typeof
import err


emit_fd = None
def emit(s):
    global emit_fd
    emit_fd.write(s)


def setup_emit(fname):
    global emit_fd
    emit_fd = open(fname, 'w')


def generate_code(symbol_table, out_fname):
    setup_emit(out_fname)

    emit(str(code.c_includes))
    forward_declare_class_obj_structs(symbol_table)
    declare_obj_structs(symbol_table)
    declare_class_structs(symbol_table)
    define_class_constructors(symbol_table)
    define_class_methods(symbol_table)
    define_the_class_struct(symbol_table)
    define_main(symbol_table)

    global emit_fd
    return emit_fd


def isbuiltin(stnode):
    return stnode.ident in ["Obj", "Int", "Boolean", "Nothing", "String"]


def symbol_table_aslist_withoutbuiltins(symbol_table):
    ret = list()
    for stnode in symbol_table.as_list():
        if stnode.ident in ["Obj", "Int", "Boolean", "Nothing", "String"]:
            continue
        else:
            ret.append(stnode)
    return ret


# Algorithm
def forward_declare_class_obj_structs(symbol_table):
    for stnode in symbol_table_aslist_withoutbuiltins(symbol_table):
        emit(code.forward_declare_structs.format(cls=stnode.ident))


def declare_obj_structs(symbol_table):
    for stnode in symbol_table_aslist_withoutbuiltins(symbol_table):
        attributes_str = ""
        for attribute in stnode.class_context.attribute_table:
            attributes_str += code.attribute.format(attr_name=attribute.name, attr_decl_type=attribute.inferred_type)
        emit(code.declare_object_struct.format(cls=stnode.ident, attributes=attributes_str))


def build_arguments_string(cls_ident, arguments):
    args_str_list = list()
    args_str_list.append(code.argument.format(type=cls_ident, name="this"))
    for argument in arguments:
        args_str_list.append(code.argument.format(type=argument.declared_type, name=argument.name))
    return code.css(args_str_list)


def build_arguments_string_constructor(cls_ident, arguments):
    args_str_list = list()
    for argument in arguments:
        args_str_list.append(code.argument.format(type=argument.declared_type, name=argument.name))
    return code.css(args_str_list)


def declare_class_structs(symbol_table):
    for stnode in symbol_table_aslist_withoutbuiltins(symbol_table):
        methods_str = ""
        constructor = stnode.class_context.constructor
        args_str = build_arguments_string_constructor(stnode.ident, constructor.arguments)
        methods_str += code.method_declaration.format(ret_type=constructor.return_type, method_name="constructor",
                                                      args=args_str)
        for method in stnode.class_context.method_table:
            this_type_ident = inherited_from(method, symbol_table)
            args_str = build_arguments_string(this_type_ident, method.arguments)
            methods_str += code.method_declaration.format(ret_type=method.return_type, method_name=method.name, args=args_str)
        emit(code.declare_class_struct.format(cls=stnode.ident, methods=methods_str))


def define_class_constructors(symbol_table):
    for stnode in symbol_table_aslist_withoutbuiltins(symbol_table):
        constructor = stnode.class_context.constructor
        args_str = build_arguments_string_constructor(stnode.ident, constructor.arguments)
        emit(code.constructor_define.format(cls=stnode.ident, args=args_str))
        emit_variable_declarations(constructor)
        emit_code_statements(constructor, symbol_table)
        emit(code.constructor_return)


def inherited_from(method, symbol_table):
    m = method
    while m.inherited:
        cls_stnode = symbol_table.find_class_stnode(m.class_ident)
        m = cls_stnode.parent.class_context.find_method(method.name)
    return m.class_ident


def find_method_inherited(method_name, cls, symbol_table):
    method = cls.find_method(method_name)
    base_cls = symbol_table.find_class(inherited_from(method, symbol_table))
    return base_cls.find_method(method.name)


def resolve_method_code_name(method, symbol_table):
    return code.method_name.format(cls=inherited_from(method, symbol_table), method_name=method.name)


def define_class_methods(symbol_table):
    for stnode in symbol_table_aslist_withoutbuiltins(symbol_table):
        for method in stnode.class_context.method_table:
            if method.inherited:
                # Don't generate the code if it is inherited
                continue
            args_str = build_arguments_string(stnode.ident, method.arguments)
            method_name_str = code.method_name.format(cls=stnode.ident, method_name=method.name)
            emit(code.method.format(ret_type=method.return_type, code_method_name=method_name_str, args=args_str))
            emit("{\n")
            emit_variable_declarations(method)
            emit_code_statements(method, symbol_table)
            if method.return_type is "Nothing":
                emit("return nothing;")
            emit("\n}\n")

def emit_variable_declarations(method):
    for var in method.variable_table:
        if var.name in [a.name for a in method.arguments] or var.name is "this":
            continue
        emit("obj_{} {};\n".format(var.declared_type, var.name))

def define_the_class_struct(symbol_table):
    def build_methods_string(cls_ident, methods):
        method_names = list()
        for method in stnode.class_context.method_table:
            method_names.append(resolve_method_code_name(method, symbol_table))
        return ",\n    ".join(method_names)

    for stnode in symbol_table_aslist_withoutbuiltins(symbol_table):
        method_names_str = build_methods_string(stnode.ident, stnode.class_context.method_table)
        emit(code.define_the_class.format(cls=stnode.ident, method_names=method_names_str))


def define_main(symbol_table):
    emit(code.define_main)


def emit_code_statements(method, st):
    gc = GetCode(method, st)
    return gc.run()


class GetCode:
    def __init__(self, method, symbol_table):
        self.m = method
        self.st = symbol_table
        self._tvar_stack = list()
        self._tvar_stash = list()
        self.count_num = 0

    def count(self):
        self.count_num += 1
        return self.count_num

    def label_gen(self, hint="label"):
        return "{}_{}".format(hint, self.count())

    def emit_label(self, label):
        emit("{}: ;\n".format(label))

    def emit_goto(self, label):
        emit("goto {};\n".format(label))

    def tvar_gen(self):
        return "tmp_{}".format(self.count())

    def tptr_gen(self):
        return "(*tmp_{})".format(self.count())

    def tvar_push(self, tvar):
        self._tvar_stack.append(tvar)

    def tvar_gen_push(self):
        tv = self.tvar_gen()
        self.tvar_push(tv)
        return tv

    def tptr_gen_push(self):
        tv = self.tptr_gen()
        self.tvar_push(tv)
        return tv

    def tvar_pop(self):
        return self._tvar_stack.pop()

    def tvar_stash(self):
        self._tvar_stash.append(self._tvar_stack)

    def tvar_restore(self):
        self._tvar_stack = self._tvar_stash.pop()

    def typeof(self, e):
        return typeof(e, self.st, self.m.variable_table)

    def run(self):
        self.emit_statements(self.m.ast_statements)

    def emit_statements(self, statements):
        for s in statements:
            self.emit_statement(s)

    def emit_statement(self, s):
        emit("/* {} */\n".format(str(s)))
        if type(s) is ast.StatementAssign:
            self.emit_expression_bottom_up(s.expression)
            if s.location.path_exp:
                self.emit_expression_bottom_up(s.location)
                ret_type = self.typeof(s.expression)
                tvar_loc = self.tvar_pop()
                tvar_val = self.tvar_pop()
                emit("{ret} = (obj_{type}){t};\n".format(ret=tvar_loc, t=tvar_val, type=ret_type))
            else:
                var_name = s.location.ident
                var = self.m.find_variable(var_name)
                emit("{ret} = (obj_{type}){t};\n".format(ret=var_name, t=self.tvar_pop(), type=var.declared_type))

        elif type(s) is ast.StatementExp:
            self.emit_expression_bottom_up(s.exp)

        elif type(s) is ast.StatementRet:
            self.emit_expression_bottom_up(s.return_exp)
            emit("return {};\n".format(self.tvar_pop()))

        elif type(s) is ast.StatementIf:
            cases = [(s.condition_exp, s.block.statements)]
            cases += [(elf.condition_exp, elf.block.statements) for elf in s.eliflist]
            if s.else_block:
                cases.append((ast.ExpressionBare("true", "Boolean"), s.else_block.statements,))
            endif_label = self.label_gen("endif")
            for condition, statements in cases:
                next_clause = self.label_gen("next")
                this_clause = self.label_gen("this")
                self.emit_bool(condition, this_clause, next_clause)
                self.emit_label(this_clause)
                self.emit_statements(statements)
                self.emit_goto(endif_label)
                self.emit_label(next_clause)
            self.emit_label(endif_label)

        elif type(s) is ast.StatementWhile:
            condition = s.condition_exp
            statements = s.block.statements
            loop_test = self.label_gen("loop_condition")
            loop_head = self.label_gen("loop_head")
            loop_end = self.label_gen("loop_end")
            self.emit_goto(loop_test)
            self.emit_label(loop_head)
            self.emit_statements(statements)
            self.emit_label(loop_test)
            self.emit_bool(condition, loop_head, loop_end)
            self.emit_label(loop_end)

        else:
            raise err.QuackGenerationError("Statement generation error: {}".format(s), s.linespan)

    def invoke_arguments(self, args):
        args_lst = list()
        for arg in args:
            self.emit_expression(arg)
        emit(", ".join(args_lst))

    # Use only for expressions
    def emit_expression_bottom_up(self, node):
        # Depth first, except short-circuit expressions
        short_circuit_expressions = [ast.ExpressionOr, ast.ExpressionAnd, ast.ExpressionNot]
        if type(node) in short_circuit_expressions:
            true_label = self.label_gen("true")
            false_label = self.label_gen("false")
            done_label = self.label_gen("done")
            self.emit_bool(node, true_label, false_label)
            self.emit_label(true_label)
            result_tvar = self.tvar_gen()
            self.tvar_push(result_tvar)
            emit("obj_Boolean {} = lit_true;\n".format(result_tvar))
            self.emit_goto(done_label)
            self.emit_label(false_label)
            emit("{} = lit_false;\n".format(result_tvar))
            self.emit_goto(done_label)
            self.emit_label(done_label)
        else:
            for child in node.children():
                self.emit_expression_bottom_up(child)
            self.emit_expression(node)

    def get_invoke_arguments(self, e, cls_name, method_name=None):
        c = self.st.find_class(cls_name)
        if method_name:
            m = find_method_inherited(method_name, c, self.st)
        else:
            m = c.constructor
        tvar_args = list()
        for i in range(len(e.invoke_args)):
            tvar_args.append("((obj_{}) {})".format(m.arguments[-(i+1)].declared_type, self.tvar_pop()))
        tvar_args.reverse()
        return tvar_args

    def emit_expression(self, e):
        if type(e) is ast.ExpressionBare:
            if type(e.value) is ast.Location:
                # Notice that this case is handled below, search for comment 'RAL - Locations Handled Here'
                pass
            else:
                if self.typeof(e) == "String":
                    output = e.value
                    # output = output.replace("\\", "\\\\")
                    output = output.replace("\n", "\\n")
                    output = output.replace("\0", "\\0")
                    output = output.replace("\b", "\\b")
                    output = output.replace("\r", "\\r")
                    output = output.replace("\f", "\\f")
                    output = output.replace("\"", "\\\"")
                    emit("obj_String {} = str_literal(\"{}\");\n".format(self.tvar_gen_push(), output))
                elif self.typeof(e) == "Int":
                    emit("obj_Int {} = int_literal({});\n".format(self.tvar_gen_push(), e.value))
                elif self.typeof(e) == "Boolean" and e.value == "false":
                    emit("obj_Boolean {} = lit_false;\n".format(self.tvar_gen_push()))
                elif self.typeof(e) == "Boolean" and e.value == "true":
                    emit("obj_Boolean {} = lit_true;\n".format(self.tvar_gen_push()))
                else:
                    raise err.QuackGenerationError("Expression generation error: {}".format(e), e.linespan)

        # RAL - Locations Handled Here
        elif type(e) is ast.Location:
            if not e.path_exp:
                # Find the variable's type
                var_name = e.ident
                var = self.m.find_variable(var_name)
                emit("obj_{} {} = {};\n".format(var.declared_type, self.tvar_gen_push(), var_name))
            else:
                # Find the attribute's type
                attr_name = e.ident
                cls = self.st.find_class(self.m.class_ident)
                attr = cls.find_attribute(attr_name)
                tvar_obj = self.tvar_pop()
                tptr_res = self.tptr_gen()
                self.tvar_push(tptr_res)
                emit("obj_{} {} = &{}->{};\n".format(attr.declared_type, tptr_res, tvar_obj, attr_name))

        elif type(e) is ast.ExpressionInvoke:
            calling_class = self.typeof(e.path_exp)
            method_name = e.method_ident
            return_type = self.typeof(e)
            # We have to get the inherited class ident to typecast into
            method = find_method_inherited(method_name, self.st.find_class(calling_class), self.st)
            tvar_caller = "((obj_{}) {})".format(method.class_ident, self.tvar_pop())
            tvar_args = self.get_invoke_arguments(e, calling_class, method_name)
            tvar_args_str = ", ".join(tvar_args)
            if len(tvar_args) == 0:
                emit("obj_{rtype} {ret} = (obj_{rtype}) {caller}->clazz->{method}({caller});\n".format(
                    rtype=return_type,
                    ret=self.tvar_gen_push(),
                    caller=tvar_caller,
                    method=method_name))
            else:
                emit("obj_{rtype} {ret} = (obj_{rtype}) {caller}->clazz->{method}({caller}, {args});\n".format(
                    rtype=return_type,
                    ret=self.tvar_gen_push(),
                    caller=tvar_caller,
                    method=method_name,
                    args=tvar_args_str))

        elif type(e) is ast.ExpressionConstructor:
            calling_class = e.ident
            tvar_args = self.get_invoke_arguments(e, calling_class)
            tvar_args_str = ", ".join(tvar_args)
            emit("obj_{cls} {ret} = the_class_{cls}->constructor({args});\n".format(
                cls=calling_class,
                ret=self.tvar_gen_push(),
                args=tvar_args_str))

        elif type(e) is ast.ExpressionBinop:
            op_map = {ast.Binops.MUL: "MULTIPLY",
                      ast.Binops.PLU: "PLUS",
                      ast.Binops.SUB: "MINUS",
                      ast.Binops.DIV: "DIVIDE"
                      }
            t2 = self.tvar_pop()
            t1 = self.tvar_pop()
            operation = op_map[e.operation]
            return_type = self.typeof(e)
            emit("obj_{rtype} {ret} = {t1}->clazz->{op}({t1}, {t2});\n".format(
                rtype=return_type,
                ret=self.tvar_gen_push(),
                t1=t1,
                op=operation,
                t2=t2))

        elif type(e) is ast.ExpressionUnaop:
            t = self.tvar_pop()
            return_type = self.typeof(e)
            emit("obj_{rtype} {ret} = {t1}->clazz->NEGATE({t1});\n".format(
                rtype=return_type,
                ret=self.tvar_gen_push(),
                t1=t))

        elif type(e) is ast.ExpressionCompare:
            op_map = {ast.Compares.LT: "LESS",
                      ast.Compares.LE: "LESSEQ",
                      ast.Compares.GT: "GREATER",
                      ast.Compares.GE: "GREATEREQ",
                      ast.Compares.EQ: "EQ"
                      }
            t2 = self.tvar_pop()
            t1 = self.tvar_pop()
            operation = op_map[e.compare]
            return_type = self.typeof(e)
            emit("obj_{rtype} {ret} = {t1}->clazz->{op}({t1}, {t2});\n".format(
                rtype=return_type,
                ret=self.tvar_gen_push(),
                t1=t1,
                op=operation,
                t2=t2))

        else:
            raise err.QuackGenerationError("Expression generation error: {}".format(e), e.linespan)

    def emit_bool(self, exp, true_label, false_label):
        if type(exp) is ast.ExpressionAnd:
            halfway = self.label_gen("and")
            self.emit_bool(exp.exp1, halfway, false_label)
            self.emit_label(halfway)
            self.emit_bool(exp.exp2, true_label, false_label)

        elif type(exp) is ast.ExpressionOr:
            halfway = self.label_gen("or")
            self.emit_bool(exp.exp1, true_label, halfway)
            self.emit_label(halfway)
            self.emit_bool(exp.exp2, true_label, false_label)

        elif type(exp) is ast.ExpressionNot:
            self.emit_bool(exp.exp, false_label, true_label)

        else:
            self.emit_expression_bottom_up(exp)
            emit("if ({} == lit_true)\n    ".format(self.tvar_pop()))
            self.emit_goto(true_label)
            self.emit_goto(false_label)

    def foo(self, e):
        # We are now handling a short circuit expression
        if type(e) is ast.ExpressionOr or type(e) is ast.ExpressionAnd:
            self.emit_bool(e, self.label_gen(), self.label_gen())
            op_map = {ast.ExpressionOr: "OR",
                      ast.ExpressionAnd: "AND"}
            t2 = self.tvar_pop()
            t1 = self.tvar_pop()
            operation = op_map[type(e)]
            return_type = self.typeof(e)
            emit("obj_{rtype} {ret} = {t1}->clazz->{op}({t1}, {t2});\n".format(
                rtype=return_type,
                ret=self.tvar_gen_push(),
                t1=t1,
                op=operation,
                t2=t2))

        elif type(e) is ast.ExpressionNot:
            t = self.tvar_pop()
            return_type = self.typeof(e)
            emit("obj_{rtype} {ret} = {t1}->clazz->NOT({t1});\n".format(
                rtype=return_type,
                ret=self.tvar_gen_push(),
                t1=t))
    #     if condition in [ast.ExpressionAnd, ast.ExpressionOr, ast.ExpressionNot]:
    #         halfway = self.label_gen("halfway")
    #         self.emit_expression_bottom_up(condition.exp1)
    #         self.emit_label(halfway)
    #         self.emit_expression_bottom_up(condition.exp2)
    #     else:
    #         self.emit_expression_bottom_up(condition)
    #         emit("if ({} == lit_true)\n    ".format(self.tvar_pop()))
    #         self.emit_goto(true_label)
    #         self.emit_goto(false_label)
    #
    #         # def init_location(self, l):
    # #     if l.path_exp:
    # #         return "this->attr_{}".format(l.ident)
    # #     else:
    # #         return "obj_{} var_{}".format(l.path_exp, l.ident)



"""
1. Forward declare class_X and obj_X structs
2. Declare the obj_x struct (include attribute table)
3. Declare the class_x struct (include method table signatures)

4. For every class:
    for every method:
        for every statement: Generate Statement Code
5, For every class in symbol table: Declare clazz singleton (e.g. ths_class_X)

10. Generate quackmain();
"""
