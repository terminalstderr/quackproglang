"""
qprs: Quack Parser
"""

import ply.yacc as yacc

from ast import *
from qlex import QuackLexer
from utility import *


class QuackParser:

    def __init__(self):
        self.lexer = None
        self.parser = None
        self.errors = list()
        self.data = None
        self.ast = None
        self.verbose = False

    def _log(self, msg):
        if self.verbose:
            print(msg, end="")

    def error(self, msg):
        self.errors.append(msg)
        eprint(msg)

    tokens = QuackLexer.tokens
    precedence = (
        ('left', 'OR'),
        ('left', 'AND'),
        ('left', 'EQUALS'),
        ('left', 'LTE', 'LT', 'GTE', 'GT'),
        ('left', 'PLUS', 'MINUS'),
        ('left', 'TIMES', 'DIVIDE'),
        ('right', 'NOT'),
        ('left', 'ACCESS'),
    )

    @staticmethod
    def ast_lineno(p):
        node = p[0]
        if isinstance(node, Node):
            node.linespan = p.linespan(0)

    def p_program(self, p):
        """ program : classlist statementlist """
        self._log("program\n")
        p[0] = Program(p[1], p[2])
        self.ast_lineno(p)

    def p_classlist(self, p):
        """ classlist   : classlist class
                        | """
        self._log("classlist\n")
        # build the class list (which is just a list of class objects)
        # If the classlist is empty then we need to make an empty list
        p[0] = self.listify(p)

    def p_statementlist(self, p):
        """ statementlist   : statementlist statement
                            | """
        self._log("statementlist\n")
        p[0] = self.listify(p)

    def p_class(self, p):
        """ class : class_sig class_body """
        self._log("class\n")
        p[0] = Class(p[1], p[2])
        self.ast_lineno(p)

    def p_class_sig(self, p):
        """ class_sig   : CLASS IDENT LPAREN arglist RPAREN
                        | CLASS IDENT LPAREN arglist RPAREN EXTENDS IDENT
                        | CLASS IDENT LPAREN arglist RPAREN EXTENDS T_OBJ"""
        self._log("class_sig\n")
        if len(p) is 6:
            p[0] = Signature(p[2], p[4], QuackObject.signature.ident)
        else:
            p[0] = Signature(p[2], p[4], p[7])
        self.ast_lineno(p)

    def p_class_body(self, p):
        """ class_body : LBRACKET statementlist methodlist RBRACKET """
        self._log("class_body\n")
        p[0] = Body(p[2], p[3])
        self.ast_lineno(p)

    def p_arglist(self, p):
        """ arglist : arglist SEP arg
                    | arg
                    | """
        self._log("arglist\n")
        p[0] = self.listify(p, self.lexer.t_SEP)
        self.ast_lineno(p)

    def p_arg(self, p):
        """ arg : IDENT TYPE IDENT
                | IDENT TYPE T_INT
                | IDENT TYPE T_OBJ
                | IDENT TYPE T_BOOLEAN
                | IDENT TYPE T_STRING """
        self._log("arg\n")
        p[0] = Argument(p[1], p[3])
        self.ast_lineno(p)

    def p_methodlist(self, p):
        """ methodlist : methodlist method
                                     | """
        self._log("methodlist\n")
        p[0] = self.listify(p)
        self.ast_lineno(p)

    def p_method(self, p):
        """ method	: DEF IDENT LPAREN arglist RPAREN statementblock
                    | DEF IDENT LPAREN arglist RPAREN TYPE IDENT statementblock
                    | DEF IDENT LPAREN arglist RPAREN TYPE T_INT statementblock
                    | DEF IDENT LPAREN arglist RPAREN TYPE T_OBJ statementblock
                    | DEF IDENT LPAREN arglist RPAREN TYPE T_BOOLEAN statementblock
                    | DEF IDENT LPAREN arglist RPAREN TYPE T_STRING statementblock """
        self._log("method\n")
        if len(p) == 7:
            p[0] = Method(p[2], p[4], p[6])
        else:
            p[0] = Method(p[2], p[4], p[8], p[7])
        self.ast_lineno(p)

    def p_statmeentblock(self, p):
        """ statementblock : LBRACKET statementlist RBRACKET """
        self._log("statementblock\n")
        p[0] = Block(p[2])
        self.ast_lineno(p)

    def p_statement(self, p):
        """ statement   : statement_if
                        | statement_while
                        | statement_return
                        | statement_assign
                        | statement_rexp"""
        self._log("statement\n")
        p[0] = p[1]
        self.ast_lineno(p)

    def p_statement_if(self, p):
        """ statement_if : IF rexp statementblock eliflist
                         | IF rexp statementblock eliflist ELSE statementblock"""
        self._log("statement_if\n")
        if len(p) == 5:
            p[0] = StatementIf(p[2], p[3], p[4])
        else:
            p[0] = StatementIf(p[2], p[3], p[4], p[6])
        self.ast_lineno(p)

    def p_eliflist(self, p):
        """ eliflist : eliflist ELIF rexp statementblock
                     | """
        self._log("eliflist")
        if len(p) == 5:
            if p[1]:
                p[0] = p[1]
                p[0].append(SubStatementElif(p[3], p[4]))
            else:
                p[0] = []
                p[0].append(SubStatementElif(p[3], p[4]))
        else:
            p[0] = []

    def p_statement_while(self, p):
        """ statement_while : WHILE rexp statementblock """
        self._log("statement_while\n")
        p[0] = StatementWhile(p[2], p[3])
        self.ast_lineno(p)

    def p_statement_return(self, p):
        """ statement_return    : RETURN EOS
                                | RETURN rexp EOS"""
        self._log("statement_return\n")
        if len(p) == 3:
            p[0] = StatementRet()
        else:
            p[0] = StatementRet(p[2])
        self.ast_lineno(p)

    def p_statement_assign(self, p):
        """ statement_assign : lexp GETS rexp EOS
                             | lexp TYPE IDENT GETS rexp EOS
                             | lexp TYPE T_INT GETS rexp EOS
                             | lexp TYPE T_BOOLEAN GETS rexp EOS
                             | lexp TYPE T_OBJ GETS rexp EOS
                             | lexp TYPE T_STRING GETS rexp EOS"""
        self._log("statement_assign")
        if len(p) == 5:
            p[0] = StatementAssign(p[1], p[3])
        elif len(p) == 7:
            p[0] = StatementAssign(p[1], p[5], p[3])
        self.ast_lineno(p)

    # How to make sure that location binds more closely than anything else?
    # Done using an "ACCESS" precidence rule at the top of this file.
    def p_lexp(self, p):
        """ lexp : IDENT
                 | rexp ACCESS IDENT"""
        if len(p) == 2:
            p[0] = Location(p[1])
        else:
            p[0] = Location(p[3], p[1])
        self.ast_lineno(p)

    def p_statement_rexp(self, p):
        """ statement_rexp : rexp EOS """
        self._log("statement_rexp\n")
        p[0] = StatementExp(p[1])
        self.ast_lineno(p)

    def p_rexp_lexp(self, p):
        """ rexp : lexp """
        self._log("rexp number\n")
        p[0] = ExpressionBare(p[1])
        self.ast_lineno(p)

    def p_rexp_bool(self, p):
        """ rexp    : TRUE
                    | FALSE"""
        self._log("rexp number\n")
        p[0] = ExpressionBare(p[1], QuackBoolean.signature.ident)
        self.ast_lineno(p)

    def p_rexp_int(self, p):
        """ rexp : NUMBER """
        self._log("rexp number\n")
        p[0] = ExpressionBare(p[1], QuackInt.signature.ident)
        self.ast_lineno(p)

    def p_rexp_string(self, p):
        """ rexp : STRING """
        self._log("rexp string\n")
        p[0] = ExpressionBare(p[1], QuackString.signature.ident)
        self.ast_lineno(p)

    def p_rexp_negate(self, p):
        """ rexp : MINUS rexp """
        self._log("rexp_negate\n")
        p[0] = ExpressionUnaop(Unaops.NEG, p[2])
        self.ast_lineno(p)

    def p_rexp_paren(self, p):
        """ rexp : LPAREN rexp RPAREN """
        self._log("rexp_paren\n")
        p[0] = p[2]
        self.ast_lineno(p)

    # TODO, we'd like a cleaner way of stripping out the escape backslash when doing this comparison...
    # if p[2] == self.lexer.t_MULT:
    # p[0] = ExpressionBinop('*',p[1],p[3])
    def p_rexp_binop1(self, p):
        """ rexp : rexp DIVIDE rexp
                 | rexp TIMES rexp """
        self._log("rexp_binop1\n")
        if p[2] == '*':
            p[0] = ExpressionBinop(Binops.MUL, p[1], p[3])
        elif p[2] == '/':
            p[0] = ExpressionBinop(Binops.DIV, p[1], p[3])
        else:
            assert False, "failed to parse"
        self.ast_lineno(p)

    def p_rexp_binop2(self, p):
        """ rexp : rexp PLUS rexp
                 | rexp MINUS rexp"""
        self._log("rexp_binop2\n")
        if p[2] == '+':
            p[0] = ExpressionBinop(Binops.PLU, p[1], p[3])
        elif p[2] == '-':
            p[0] = ExpressionBinop(Binops.SUB, p[1], p[3])
        else:
            assert False, "failed to parse"
        self.ast_lineno(p)

    def p_rexp_not(self, p):
        """ rexp : NOT rexp """
        self._log("rexp_not\n")
        p[0] = ExpressionNot(p[2])
        self.ast_lineno(p)

    def p_rexp_and(self, p):
        """ rexp : rexp AND rexp """
        self._log("rexp_and\n")
        p[0] = ExpressionAnd(p[1], p[3])
        self.ast_lineno(p)

    def p_rexp_or(self, p):
        """ rexp : rexp OR rexp """
        self._log("rexp_or\n")
        p[0] = ExpressionOr(p[1], p[3])
        self.ast_lineno(p)

    def p_rexp_compare(self, p):
        """ rexp : rexp EQUALS rexp
                 | rexp LTE rexp
                 | rexp LT rexp
                 | rexp GTE rexp
                 | rexp GT rexp"""
        self._log("rexp_compare\n")
        if p[2] == self.lexer.t_EQUALS:
            p[0] = ExpressionCompare(Compares.EQ, p[1], p[3])
        elif p[2] == self.lexer.t_LTE:
            p[0] = ExpressionCompare(Compares.LE, p[1], p[3])
        elif p[2] == self.lexer.t_LT:
            p[0] = ExpressionCompare(Compares.LT, p[1], p[3])
        elif p[2] == self.lexer.t_GTE:
            p[0] = ExpressionCompare(Compares.GE, p[1], p[3])
        elif p[2] == self.lexer.t_GT:
            p[0] = ExpressionCompare(Compares.GT, p[1], p[3])
        else:
            pass  # TODO
        self.ast_lineno(p)

    def p_rexp_invoke(self, p):
        """ rexp : rexp ACCESS IDENT LPAREN invokearglist RPAREN """
        p[0] = ExpressionInvoke(p[1], p[3], p[5])
        self.ast_lineno(p)

    def p_rexp_invoke_constructor(self, p):
        """ rexp    : IDENT LPAREN invokearglist RPAREN
                    | T_INT LPAREN invokearglist RPAREN
                    | T_OBJ LPAREN invokearglist RPAREN
                    | T_STRING LPAREN invokearglist RPAREN
                    | T_BOOLEAN LPAREN invokearglist RPAREN"""
        p[0] = ExpressionConstructor(p[1], p[3])
        self.ast_lineno(p)

    def p_invokearglist(self, p):
        """ invokearglist   : invokearglist SEP invokearg
                            | invokearg
                            | """
        self._log("invokearglist\n")
        p[0] = self.listify(p, self.lexer.t_SEP)
        self.ast_lineno(p)

    def p_invokearg(self, p):
        """ invokearg : rexp"""
        self._log("invokearg\n")
        p[0] = p[1]
        self.ast_lineno(p)

    def p_error(self, p):
        if p:
            self.error_generic(p)
            self._log("parse error err#{}\n".format(len(self.errors)))
        else:
            self.error("Error at end of file... Missing ';' or '}' maybe?")
            self._log("parse error err#{}\n".format(len(self.errors)))

    @staticmethod
    def listify(p, sep=None):
        ret = []
        if sep:
            ret = QuackParser.get_seperated_list(p)
        else:
            if len(p) is not 1:
                if p[1] is None:
                    ret.append(p[2])
                else:
                    ret = p[1]
                    ret.append(p[2])
        return ret

    @staticmethod
    def get_seperated_list(p):
        """ arglist : arglist SEP arg
                    | arg
                    | """
        if len(p) == 1:
            return []
        elif len(p) == 4 and p[1] is None:
            ret = []
            ret.append(p[3])
            return ret
        elif len(p) == 4 and p[1] is not None:
            ret = p[1]
            ret.append(p[3])
            return ret
        else:  # len(p) == 2
            ret = []
            ret.append(p[1])
            return ret

    def geterrorline(self, token):
        start = self.data.rfind('\n', 0, token.lexpos)
        end = self.data.find('\n', token.lexpos, len(self.data))
        if start == -1:
            start = 0
        if end == -1:
            end = 0
        errorline = self.data[start:end]
        pointer = " " * (token.lexpos - start - 1) + "^"
        return errorline + "\n" + pointer

    def error_generic(self, p):
        self.error(
            "err#{} -- {}: Syntax Error when parsing {} '{}'\n{}\n".format(len(self.errors) + 1, p.lineno, p.type,
                                                                           p.value, self.geterrorline(p)))

    def build(self, **kwargs):
        # Build the lexer for our parser to use -- it must be named 'self.lexer' (discoverd by experimentation)
        self.lexer = QuackLexer()
        self.lexer.build()
        self.parser = yacc.yacc(module=self, **kwargs)

    def process(self, data):
        self.data = data
        self.ast = self.parser.parse(data, tracking=True)
        if len(self.lexer.errors) > 0 or len(self.errors) > 0:
            return -1
        return 0
