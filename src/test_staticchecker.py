from staticchecker import *
from hamcrest import *
from testutil import *
from nose.tools import assert_raises


def test_buildSkeletonSymtable_SqrDecl_findclass():
    ast_program = parse_file(trials_path + "SqrDecl.qk")

    # ACT
    sst = build_skeleton_symbol_table(ast_program)

    # ASSERTIONS Obj
    obj_stnode = sst.find_class_stnode("Obj")
    assert_that(obj_stnode.parent, none())
    # 2 defined, 1 for "Main", 4 builtin
    assert_that(obj_stnode.children, has_length(2 + 1 + 4))

    # ASSERTIONS "Main"
    main_stnode = sst.find_class_stnode("Main")
    assert_that(main_stnode.ident, is_("Main"))
    assert_that(main_stnode.parent.ident, is_("Obj"))
    assert_that(main_stnode.parent, is_(obj_stnode))

    # ASSERTIONS Pt
    pt_stnode = sst.find_class_stnode("Pt")
    assert_that(pt_stnode.ident, is_("Pt"))
    assert_that(pt_stnode.parent, is_(obj_stnode))

    # ASSERTIONS Rect
    rect_stnode = sst.find_class_stnode("Rect")
    assert_that(rect_stnode.ident, is_("Rect"))
    assert_that(rect_stnode.parent, is_(obj_stnode))

    # ASSERTIONS Square
    sqr_stnode = sst.find_class_stnode("Square")
    assert_that(sqr_stnode.ident, is_("Square"))
    assert_that(sqr_stnode.parent, is_not(obj_stnode))
    assert_that(sqr_stnode.parent, is_(rect_stnode))


def test_buildSkeletonSymtable_SqrDecl_lca():
    ast_program = parse_file(trials_path + "SqrDecl.qk")

    # ACT
    sst = build_skeleton_symbol_table(ast_program)

    # ASSERTIONS Obj
    obj_stnode = sst.find_class_stnode("Obj")
    main_stnode = sst.find_class_stnode("Main")
    pt_stnode = sst.find_class_stnode("Pt")
    rect_stnode = sst.find_class_stnode("Rect")
    sqr_stnode = sst.find_class_stnode("Square")

    assert_that(obj_stnode.lca(main_stnode), is_(obj_stnode))
    assert_that(pt_stnode.lca(main_stnode), is_(obj_stnode))
    assert_that(rect_stnode.lca(main_stnode), is_(obj_stnode))
    assert_that(sqr_stnode.lca(pt_stnode), is_(obj_stnode))

    assert_that(rect_stnode.lca(sqr_stnode), is_(rect_stnode))
    assert_that(sqr_stnode.lca(rect_stnode), is_(rect_stnode))


def test_buildSkeletonSymtable_SqrDecl_supersubtypecheck():
    ast_program = parse_file(trials_path + "SqrDecl.qk")

    # ACT
    sst = build_skeleton_symbol_table(ast_program)

    # ASSERTIONS Obj
    obj_stnode = sst.find_class_stnode("Obj")
    main_stnode = sst.find_class_stnode("Main")
    pt_stnode = sst.find_class_stnode("Pt")
    rect_stnode = sst.find_class_stnode("Rect")
    sqr_stnode = sst.find_class_stnode("Square")

    assert_that(obj_stnode.is_superclass_of(obj_stnode))
    assert_that(rect_stnode.is_superclass_of(rect_stnode))
    assert_that(sqr_stnode.is_superclass_of(sqr_stnode))
    assert_that(obj_stnode.is_subclass_of(obj_stnode))
    assert_that(rect_stnode.is_subclass_of(rect_stnode))
    assert_that(sqr_stnode.is_subclass_of(sqr_stnode))

    assert_that(obj_stnode.is_superclass_of(main_stnode))
    assert_that(obj_stnode.is_superclass_of(pt_stnode))
    assert_that(obj_stnode.is_superclass_of(sqr_stnode))
    assert_that(obj_stnode.is_superclass_of(rect_stnode))

    assert_that(rect_stnode.is_superclass_of(sqr_stnode))
    assert_that(sqr_stnode.is_subclass_of(rect_stnode))


def test_buildSkeletonSymtable_SqrDecl_aslist():
    ast_program = parse_file(trials_path + "SqrDecl.qk")

    # ACT
    sst = build_skeleton_symbol_table(ast_program)

    # ASSERTIONS Obj
    lst = sst.as_list()
    assert_that(lst, has_length(9))
    assert_that(lst[0].ident, is_("Obj"))
    assert_that(lst[-1].ident, is_("Square"))


def test_buildAttributedSymtable_SqrDecl():
    ast_program = parse_file(trials_path + "SqrDecl.qk")
    st = build_skeleton_symbol_table(ast_program)

    # ACT
    build_attribute_tables(st)

    pt_stnode = st.find_class_stnode("Pt")
    pt_attributes = pt_stnode.class_context.attribute_table
    assert_that(pt_attributes, has_length(2))
    assert_that(pt_attributes[0].name, is_("x"))
    assert_that(pt_attributes[1].name, is_("y"))
    assert_that(pt_attributes[0].declared_type, is_("Int"))
    assert_that(pt_attributes[1].declared_type, is_("Int"))

    rect_stnode = st.find_class_stnode("Rect")
    rect_attributes = rect_stnode.class_context.attribute_table
    assert_that(rect_attributes, has_length(2))
    assert_that(rect_attributes[0].name, is_("ll"))
    assert_that(rect_attributes[1].name, is_("ur"))
    assert_that(rect_attributes[0].declared_type, is_("Pt"))
    assert_that(rect_attributes[1].declared_type, is_("Pt"))

    sqr_stnode = st.find_class_stnode("Square")
    sqr_attributes = sqr_stnode.class_context.attribute_table
    assert_that(sqr_attributes, has_length(2))
    assert_that(sqr_attributes[0].name, is_("ll"))
    assert_that(sqr_attributes[1].name, is_("ur"))
    assert_that(sqr_attributes[0].declared_type, is_("Pt"))
    assert_that(sqr_attributes[1].declared_type, is_("Pt"))


def test_buildConstructors_SqrDecl():
    ast_program = parse_file(trials_path + "SqrDecl.qk")
    st = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(st)

    # ACT
    build_constructors(st)

    # ASSERT
    obj_stnode = st.find_class_stnode("Obj")
    assert_that(obj_stnode.class_context.constructor, not_none())
    assert_that(obj_stnode.class_context.constructor.name, is_("Obj"))
    assert_that(obj_stnode.class_context.constructor.return_type, is_("Obj"))

    rect_stnode = st.find_class_stnode("Rect")
    constr = rect_stnode.class_context.constructor
    assert_that(constr, not_none())
    assert_that(constr.name, is_("Rect"))
    assert_that(constr.return_type, is_("Rect"))
    assert_that(constr.arguments[0].declared_type, is_("Pt"))
    assert_that(constr.arguments[0].name, is_("ll"))
    assert_that(constr.arguments[1].declared_type, is_("Pt"))
    assert_that(constr.arguments[1].name, is_("ur"))

    sqr_stnode = st.find_class_stnode("Square")
    constr = sqr_stnode.class_context.constructor
    assert_that(constr, not_none())
    assert_that(constr.name, is_("Square"))
    assert_that(constr.return_type, is_("Square"))
    assert_that(constr.arguments[0].declared_type, is_("Pt"))
    assert_that(constr.arguments[0].name, is_("ll"))
    assert_that(constr.arguments[1].declared_type, is_("Int"))
    assert_that(constr.arguments[1].name, is_("side"))


def test_buildSkeletonMethodTable_SqrDecl():
    ast_program = parse_file(trials_path + "SqrDecl.qk")
    st = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(st)
    build_constructors(st)

    # ACT
    build_skeleton_method_tables(st)

    obj_stnode = st.find_class_stnode("Obj")
    obj_methods = obj_stnode.class_context.method_table
    assert_that(obj_methods, has_length(3))
    assert_that(obj_methods[0].name, is_("PRINT"))
    assert_that(obj_methods[1].name, is_("STR"))
    assert_that(obj_methods[2].name, is_("EQ"))

    rect_stnode = st.find_class_stnode("Rect")
    rect_methods = rect_stnode.class_context.method_table
    assert_that(rect_methods, has_length(4))
    assert_that(rect_methods[0].name, is_("PRINT"))
    assert_that(rect_methods[1].name, is_("STR"))
    assert_that(rect_methods[2].name, is_("EQ"))
    assert_that(rect_methods[3].name, is_("translate"))

    sqr_stnode = st.find_class_stnode("Square")
    sqr_methods = sqr_stnode.class_context.method_table
    assert_that(sqr_methods, has_length(4))
    assert_that(sqr_methods[0].name, is_("PRINT"))
    assert_that(sqr_methods[1].name, is_("STR"))
    assert_that(sqr_methods[2].name, is_("EQ"))
    assert_that(sqr_methods[3].name, is_("translate"))

    pt_stnode = st.find_class_stnode("Pt")
    pt_methods = pt_stnode.class_context.method_table
    assert_that(pt_methods, has_length(6))
    assert_that(pt_methods[0].name, is_("PRINT"))
    assert_that(pt_methods[1].name, is_("STR"))
    assert_that(pt_methods[2].name, is_("EQ"))
    assert_that(pt_methods[3].name, is_("PLUS"))
    assert_that(pt_methods[4].name, is_("_x"))
    assert_that(pt_methods[5].name, is_("_y"))

    ptplus = pt_methods[3]
    ptequals = pt_methods[2]
    assert_that(ptequals.arguments[0].declared_type, is_("Obj"))
    assert_that(ptequals.return_type, is_("Boolean"))
    assert_that(ptplus.arguments[0].declared_type, is_("Pt"))
    assert_that(ptplus.return_type, is_("Pt"))

    recttrans = rect_methods[3]
    assert_that(recttrans.return_type, is_("Rect"))
    assert_that(recttrans.arguments[0].declared_type, is_("Pt"))

    sqrtrans = sqr_methods[3]
    assert_that(sqrtrans.return_type, is_("Rect"))
    assert_that(sqrtrans.arguments[0].declared_type, is_("Pt"))


# ensure that code_method_name is correct for each
def test_buildSkeletonMethodTable2_SqrDecl():
    ast_program = parse_file(trials_path + "SqrDecl.qk")
    st = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(st)
    build_constructors(st)

    # ACT
    build_skeleton_method_tables(st)

    # ensure that 'inherited' is set correctly for each
    obj_stnode = st.find_class_stnode("Obj")
    obj_methods = obj_stnode.class_context.method_table
    assert_that(not obj_methods[0].inherited)
    assert_that(not obj_methods[1].inherited)
    assert_that(not obj_methods[2].inherited)

    rect_stnode = st.find_class_stnode("Rect")
    rect_methods = rect_stnode.class_context.method_table
    assert_that(rect_methods[0].inherited)
    assert_that(not rect_methods[1].inherited)
    assert_that(rect_methods[2].inherited)
    assert_that(not rect_methods[3].inherited)

    sqr_stnode = st.find_class_stnode("Square")
    sqr_methods = sqr_stnode.class_context.method_table
    assert_that(sqr_methods[0].inherited)
    assert_that(sqr_methods[1].inherited)
    assert_that(sqr_methods[2].inherited)
    assert_that(sqr_methods[3].inherited)

    pt_stnode = st.find_class_stnode("Pt")
    pt_methods = pt_stnode.class_context.method_table
    assert_that(pt_methods[0].inherited)
    assert_that(not pt_methods[1].inherited)
    assert_that(pt_methods[2].inherited)
    assert_that(not pt_methods[3].inherited)
    assert_that(not pt_methods[4].inherited)
    assert_that(not pt_methods[5].inherited)



def test_buildFullMethodTable_SqrDecl():
    ast_program = parse_file(trials_path + "SqrDecl.qk")
    st = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(st)
    build_constructors(st)
    build_skeleton_method_tables(st)

    # ACT
    fill_method_tables(st)

    # ASSERT
    obj_stnode = st.find_class_stnode("Obj")
    obj_methods = obj_stnode.class_context.method_table
    objeq_vars = obj_methods[2].variable_table
    assert_that(objeq_vars, has_length(2))
    assert_that(objeq_vars[0].declared_type, is_("Obj"))
    assert_that(objeq_vars[0].name, is_("this"))
    assert_that(objeq_vars[1].declared_type, is_("Obj"))

    rect_stnode = st.find_class_stnode("Rect")
    rect_methods = rect_stnode.class_context.method_table
    rectstr_vars = rect_methods[1].variable_table
    assert_that(rectstr_vars, has_length(3))
    assert_that(rectstr_vars[0].declared_type, is_("Rect"))
    assert_that(rectstr_vars[0].name, is_("this"))
    assert_that(rectstr_vars[1].declared_type, is_("Pt"))
    assert_that(rectstr_vars[1].name, is_("lr"))
    assert_that(rectstr_vars[2].declared_type, is_("Pt"))
    assert_that(rectstr_vars[2].name, is_("ul"))


def test_buildFullMethodTable_SqrDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(st)
    build_constructors(st)
    build_skeleton_method_tables(st)

    # ACT
    fill_method_tables(st)

    # ASSERT
    rect_stnode = st.find_class_stnode("Rect")
    rect_methods = rect_stnode.class_context.method_table
    rectquad = rect_methods[4]
    rectcheck = rect_methods[5]

    assert_that(rectquad.name, is_("getQuad"))
    assert_that(rectquad.variable_table, has_length(2))
    assert_that(rectquad.variable_table[0].name, is_("this"))
    assert_that(rectquad.variable_table[1].name, is_("quad"))

    assert_that(rectcheck.name, is_("check"))
    assert_that(rectcheck.variable_table, has_length(2))
    assert_that(rectcheck.variable_table[0].name, is_("this"))
    assert_that(rectcheck.variable_table[1].name, is_("flag"))
    assert_that(rectcheck.variable_table[1].declared_type, is_("Boolean"))


def test_buildFullMethodTable2_SqrDeclComplex_():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(st)
    build_constructors(st)
    build_skeleton_method_tables(st)

    # ACT
    fill_method_tables(st)

    # ASSERT
    rect_stnode = st.find_class_stnode("Rect")
    rect_methods = rect_stnode.class_context.method_table
    rectstr_vars = rect_methods[4].variable_table
    assert_that(rectstr_vars[0].declared_type, is_("Rect"))
    assert_that(rectstr_vars[0].name, is_("this"))

    sqr_stnode = st.find_class_stnode("Square")
    sqr_methods = sqr_stnode.class_context.method_table
    sqrstr_vars = sqr_methods[4].variable_table
    assert_that(sqrstr_vars[0].declared_type, is_("Square"))
    assert_that(sqrstr_vars[0].name, is_("this"))


def test_inferredTypes_SqrDecl():
    ast_program = parse_file(trials_path + "SqrDecl.qk")
    st = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(st)
    build_constructors(st)
    build_skeleton_method_tables(st)
    fill_method_tables(st)

    # ACT
    infer_variable_types(st)

    # ASSERT
    obj_stnode = st.find_class_stnode("Obj")
    obj_methods = obj_stnode.class_context.method_table
    objeq_vars = obj_methods[2].variable_table
    assert_that(objeq_vars, has_length(2))
    assert_that(objeq_vars[0].inferred_type, is_("Obj"))
    assert_that(objeq_vars[0].name, is_("this"))
    assert_that(objeq_vars[1].inferred_type, is_("Obj"))

    rect_stnode = st.find_class_stnode("Rect")
    rect_methods = rect_stnode.class_context.method_table
    rectstr_vars = rect_methods[1].variable_table
    assert_that(rectstr_vars, has_length(3))
    assert_that(rectstr_vars[0].inferred_type, is_("Rect"))
    assert_that(rectstr_vars[0].name, is_("this"))
    assert_that(rectstr_vars[1].inferred_type, is_("Pt"))
    assert_that(rectstr_vars[1].name, is_("lr"))
    assert_that(rectstr_vars[2].inferred_type, is_("Pt"))
    assert_that(rectstr_vars[2].name, is_("ul"))


def test_checkUndeclaredMethod_SqrDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_undefined_method_call(st)


def test_checkUndeclaredMethod_neg19():
    ast_program = parse_file(neg_test_path + "19_callUndeclaredMethod.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_undefined_method_call(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("_z"))
    assert_that(ex.msg, contains_string("Pt"))
    assert_that(ex.msg, contains_string("Unable to find method"))


def test_checkUndeclaredVariable_SqlDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_uninitialized_variable_usage(st)


def test_checkUndeclaredVariable_neg09():
    ast_program = parse_file(neg_test_path + "09_usageBeforeInitialization.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_uninitialized_variable_usage(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("u"))
    assert_that(ex.msg, contains_string("used without being initialized"))


def test_checkUndeclaredVariable_neg20():
    ast_program = parse_file(neg_test_path + "20_useUndeclaredVariable.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_uninitialized_variable_usage(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("used without being initialized"))


def test_checkUndeclaredVariable_neg21():
    ast_program = parse_file(neg_test_path + "21_useUndeclaredVariable.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_uninitialized_variable_usage(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("flag"))
    assert_that(ex.msg, contains_string("used without being initialized"))


def test_checkAttemptPublicAccess_SqlDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_attempt_public_access(st)


def test_checkAttemptPublicAccess_neg23():
    ast_program = parse_file(neg_test_path + "23_attemptPublicAccess.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_attempt_public_access(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("x"))
    assert_that(ex.msg, contains_string("Pt"))
    assert_that(ex.msg, contains_string("Attempted to access protected attribute"))


def test_checkAttemptPublicAccess_neg24():
    ast_program = parse_file(neg_test_path + "24_attemptPublicAccess.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_attempt_public_access(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("x"))
    assert_that(ex.msg, contains_string("Pt"))
    assert_that(ex.msg, contains_string("Attempted to access protected attribute"))


def test_checkUndeclaredAttribute_SqrDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_undefined_attribute_usage(st)



def test_checkUndeclaredAttribute_neg22():
    ast_program = parse_file(neg_test_path + "22_useUndeclaredAttribute.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_undefined_attribute_usage(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("z"))
    assert_that(ex.msg, contains_string("Pt"))
    assert_that(ex.msg, contains_string("Used nonexistant attribute"))


def test_checkMethodOverriding_SqlDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_method_overriding(st)


def test_checkMethodOverriding_neg25():
    ast_program = parse_file(neg_test_path + "25_methodOverrideBadReturn.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_method_overriding(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Obj"))
    assert_that(ex.msg, contains_string("Pt"))
    assert_that(ex.msg, contains_string("Spt::whoami"))
    assert_that(ex.msg, contains_string("Pt::whoami"))
    assert_that(ex.msg, contains_string("Method override with non-covariant return type"))


def test_checkMethodOverriding_neg26():
    ast_program = parse_file(neg_test_path + "26_methodOverrideBadArg.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_method_overriding(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Spt"))
    assert_that(ex.msg, contains_string("Pt"))
    assert_that(ex.msg, contains_string("Spt::sum"))
    assert_that(ex.msg, contains_string("Pt::sum"))
    assert_that(ex.msg, contains_string("Method override with non-contravariant argument type"))


def test_checkMethodOverriding_neg27():
    ast_program = parse_file(neg_test_path + "27_methodOverrideBadNumArgs.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_method_overriding(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Spt::PLUS"))
    assert_that(ex.msg, contains_string("Pt::PLUS"))
    assert_that(ex.msg, contains_string("Method override with different number of arguments"))


def test_checkConditionalsAreBool_SqlDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_type_conditional_expressions(st)


def test_checkConditionalsAreBool_neg29():
    ast_program = parse_file(neg_test_path + "29_typecheckBooleanAnd.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("and/or"))


def test_checkConditionalsAreBool_neg30():
    ast_program = parse_file(neg_test_path + "30_typecheckBooleanOr.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("and/or"))


def test_checkConditionalsAreBool_neg31():
    ast_program = parse_file(neg_test_path + "31_typecheckBooleanNot.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("not"))


def test_checkConditionalsAreBool_neg32():
    ast_program = parse_file(neg_test_path + "32_typecheckBooleanIf.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("passed as condition"))


def test_checkConditionalsAreBool_neg33():
    ast_program = parse_file(neg_test_path + "33_typecheckBooleanElif.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("passed as condition"))


def test_checkConditionalsAreBool_neg34():
    ast_program = parse_file(neg_test_path + "34_typecheckBooleanWhile.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("passed as condition"))


def test_checkConditionalsAreBool_neg35():
    ast_program = parse_file(neg_test_path + "35_typecheckBooleanNested.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("passed as condition"))


def test_checkConditionalsAreBool_neg36():
    ast_program = parse_file(neg_test_path + "36_typecheckBooleanInConstructor.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("passed as condition"))


def test_checkConditionalsAreBool_neg37():
    ast_program = parse_file(neg_test_path + "37_typecheckBooleanInMethod.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_conditional_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("y"))
    assert_that(ex.msg, contains_string("Non-Boolean"))
    assert_that(ex.msg, contains_string("passed as condition"))


def test_checkMethodInvocationTypes_SqlDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_type_method_invocation(st)


def test_checkMethodInvocationTypes_neg38():
    ast_program = parse_file(neg_test_path + "38_typecheckMethodInvocation.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_method_invocation(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::foo"))
    assert_that(ex.msg, contains_string("expected object of type"))
    assert_that(ex.msg, contains_string("Int"))
    assert_that(ex.msg, contains_string("Obj"))


def test_checkMethodInvocationTypes_neg39():
    ast_program = parse_file(neg_test_path + "39_typecheckMethodInvocation.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_method_invocation(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::foo"))
    assert_that(ex.msg, contains_string("expected object of type"))
    assert_that(ex.msg, contains_string("Boolean"))
    assert_that(ex.msg, contains_string("Int"))


def test_checkMethodInvocationTypes_neg40():
    ast_program = parse_file(neg_test_path + "40_typecheckMethodInvocation.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_method_invocation(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::foo"))
    assert_that(ex.msg, contains_string("expected object of type"))
    assert_that(ex.msg, contains_string("Boolean"))
    assert_that(ex.msg, contains_string("Int"))


def test_checkMethodInvocationTypes_neg41():
    ast_program = parse_file(neg_test_path + "41_typecheckMethodInvocation.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_method_invocation(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::foo"))
    assert_that(ex.msg, contains_string("expected object of type"))
    assert_that(ex.msg, contains_string("Boolean"))
    assert_that(ex.msg, contains_string("Int"))


def test_checkMethodInvocationTypes_neg42():
    ast_program = parse_file(neg_test_path + "42_typecheckMethodInvocation.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_method_invocation(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::foo"))
    assert_that(ex.msg, contains_string("expected object of type"))
    assert_that(ex.msg, contains_string("Boolean"))
    assert_that(ex.msg, contains_string("Int"))


def test_checkMethodInvocationTypes_neg43():
    ast_program = parse_file(neg_test_path + "43_typecheckMethodInvocation.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_method_invocation(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::foo"))
    assert_that(ex.msg, contains_string("expected object of type"))
    assert_that(ex.msg, contains_string("Boolean"))
    assert_that(ex.msg, contains_string("argument position 2"))
    assert_that(ex.msg, contains_string("Int"))


def test_checkReturnTypes_SqlDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_type_return(st)


def test_checkReturnTypes_neg44():
    ast_program = parse_file(neg_test_path + "44_typecheckReturnType.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_return(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::some_int"))
    assert_that(ex.msg, contains_string("expects return expression of type"))
    assert_that(ex.msg, contains_string("Int"))
    assert_that(ex.msg, contains_string("Boolean"))


def test_checkReturnTypes_neg45():
    ast_program = parse_file(neg_test_path + "45_typecheckReturnType.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_return(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::some_int"))
    assert_that(ex.msg, contains_string("expects return expression of type"))
    assert_that(ex.msg, contains_string("Int"))
    assert_that(ex.msg, contains_string("Obj"))


def test_checkReturnTypes_neg46():
    ast_program = parse_file(neg_test_path + "46_typecheckReturnType.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_return(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::some_int"))
    assert_that(ex.msg, contains_string("expects return expression of type"))
    assert_that(ex.msg, contains_string("Int"))
    assert_that(ex.msg, contains_string("Boolean"))


def test_checkTypeThisClass_SqlDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    check_type_path_expressions(st)


def test_checkTypeThisClass_neg47():
    ast_program = parse_file(neg_test_path + "47_typecheckAccessOtherAttribute.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_path_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::copy"))
    assert_that(ex.msg, contains_string("expect path to be of type"))
    assert_that(ex.msg, contains_string("Test"))
    assert_that(ex.msg, contains_string("Other"))


def test_checkTypeThisClass_neg48():
    ast_program = parse_file(neg_test_path + "48_typecheckAccessOtherAttribute.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_path_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::Test"))
    assert_that(ex.msg, contains_string("expect path to be of type"))
    assert_that(ex.msg, contains_string("Test"))
    assert_that(ex.msg, contains_string("Other"))


def test_checkTypeThisClass_neg49():
    ast_program = parse_file(neg_test_path + "49_typecheckAccessOtherAttribute.qk")
    st = build_symbol_table(ast_program)

    # ACT
    with assert_raises(err.QuackCheckError) as cm:
        check_type_path_expressions(st)
    ex = cm.exception

    # ASSERT
    assert_that(ex.msg, contains_string("Test::copy"))
    assert_that(ex.msg, contains_string("expect path to be of type"))
    assert_that(ex.msg, contains_string("Test"))
    assert_that(ex.msg, contains_string("Other"))


def test_runChecks_SqrDeclComplex():
    ast_program = parse_file(trials_path + "SqrDeclComplex.qk")
    st = build_symbol_table(ast_program)

    # ACT
    run_checks(st)
