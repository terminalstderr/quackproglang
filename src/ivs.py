"""
ivs: InitializedVariables
"""
import ast
import err
import symtable as sym


class InitializedVariables:
    """
    A simple container with some convenience methods. Contains a list of definitely initialized variables 'initialized'
    and a list of possibly initialized variables 'possibly_initialized'
    """

    def __init__(self, initialized=list(), possibly_initialized=list()):
        self.initialized = list(initialized)
        self.possibly_initialized = list(possibly_initialized)
        self.cls_ivs = None

    def is_possibly_initialized(self, var):
        """
        Based on this InitializedVariables structure, determine whether a variable is in the possibly_initialized list
        :param var: an AST Location that we are trying to find
        :return: True if that AST Location was found in this structure's possibly initialized variables
        """
        return var.ident in [iv.name for iv in self.possibly_initialized]

    def is_initialized(self, var):
        """
        Based on this InitializedVariables structure, determine whether a variable is initialized or not
        :param var: an AST Location that we are trying to find
        :return: True if that AST Location was found in this structure's initialized variables
        """
        if var.path_exp is not None:
            # TODO check if the path_exp is typeof "this class" (otherwise throw exception)
            if not self.cls_ivs:
                pass  # TODO raise an exception
            return var.ident in [iv.name for iv in self.cls_ivs.initialized]
        return var.ident in [iv.name for iv in self.initialized]

    def add_iv(self, initialized_variables):
        self.initialized += initialized_variables.initialized
        self.possibly_initialized += initialized_variables.possibly_initialized

    def add(self, initialized, possibly_initialized):
        """
        Adds a
        :param initialized: either a single initialized variable or a list of initialized variables
        :param possibly_initialized: either a single possibly_initialized variable or a list of possibly_initialized
        variables
        :return: None
        """
        if isinstance(initialized, list) and isinstance(possibly_initialized, list):
            self.initialized += initialized
            self.possibly_initialized += possibly_initialized
        else:
            self.initialized.append(initialized)
            self.possibly_initialized.append(possibly_initialized)

    def remove_redundant(self):
        """
        Removes redundant entries from the internal data structures
        :return: None
        """
        self.initialized = self._variable_list_remove_duplicates(self.initialized)
        self.possibly_initialized = self._variable_list_remove_duplicates(self.possibly_initialized)
        self.possibly_initialized = self._variable_list_remove_overlap(self.possibly_initialized, self.initialized)

    def intersection(self, other):
        """
        Only variables initialized in both set of IVs will be put into the initialized list of this set. Everything else
        goes into the possibly_initialized set
        :param other:
        :return:
        """
        # Calculate the intersection and difference between self and other.
        intersection, difference = self.list_intersection(self.initialized, other.initialized)
        # Put the intersection into this initialized list.
        self.initialized = intersection
        # Put everything else into the possibly_initialized list.
        self.possibly_initialized += difference
        self.possibly_initialized += other.possibly_initialized

    @staticmethod
    def list_intersection(l1, l2):
        intersection = list()
        difference = list()
        # TODO when getting the intersection, need to make sure
        for var1 in l1:
            if var1.name in [var2.name for var2 in l2]:
                intersection.append(var1)
            else:
                difference.append(var1)
        for var2 in l2:
            if var2.name in [var1.name for var1 in l1]:
                pass
                # the intersection will already contain this item
            else:
                difference.append(var2)
        return intersection, difference

    @staticmethod
    def _variable_list_remove_duplicates(lst):
        # check
        names = []
        ret = []
        for var in lst:
            if var.name not in names:
                ret.append(var)
                names.append(var.name)
        return ret

    @staticmethod
    def _variable_list_remove_overlap(lst1, lst2):
        """
        Removes any overlap from lst1, we result in a copy of lst1 that does not contain any of the statements found in
        lst2
        :param lst1: the origin list
        :param lst2: the list of elements to ensure we do not have
        :return: a version of lst1 without any of the statements from lst2
        """
        lst2_names = [assn_smnt.name for assn_smnt in lst2]
        ret = []
        for item in lst1:
            if item.name not in lst2_names:
                ret.append(item)
        return ret


"""
Check if a class has an attribute
"""

"""
In this class, we check that a list of statements only makes use of initialized variables.
The list of initialized variables is known ahead of time.
We walk through every statement and make sure the
"""
class GetUsedVariables:
    def __init__(self):
        self.used = list()

    @staticmethod
    def get_used(statements):
        guv = GetUsedVariables()
        guv.get_statement_list_used(statements)
        return guv.used

    @staticmethod
    def get_used_variable_name_linespan_list(statements):
        used_locations = GetUsedVariables.get_used(statements)
        ret = list()
        for location in used_locations:
            while location.path_exp:
                location = location.path_exp.value
            ret.append((location.ident, location.linespan))
        return ret

    @staticmethod
    def get_used_attribute_name_linespan_list(statements):
        used_locations = GetUsedVariables.get_used(statements)
        ret = list()
        for loc in used_locations:
            if (loc.ident != "this") and loc.path_exp:
                ret.append((loc.ident, loc.linespan))
        return ret

    def get_statement_list_used(self, statements):
        for statement in statements:
            self.get_statement_used(statement)

    def get_statement_used(self, statement):
        if type(statement) is ast.StatementIf:
            self.used += self.get_expression_used(statement.condition_exp)
            if statement.else_block:
                self.get_statement_list_used(statement.else_block.statements)
            for elif_statement in statement.eliflist:
                self.used += self.get_expression_used(elif_statement.condition_exp)
                self.get_statement_list_used(elif_statement.block.statements)

        elif type(statement) is ast.StatementWhile:
            self.used += self.get_expression_used(statement.condition_exp)
            self.get_statement_list_used(statement.block.statements)

        elif type(statement) is ast.StatementExp:
            self.used += self.get_expression_used(statement.exp)

        elif type(statement) is ast.StatementAssign:
            self.used += self.get_expression_used(statement.expression)

        elif type(statement) is ast.StatementRet:
            self.used += self.get_expression_used(statement.return_exp)

        else:
            raise err.QuackInternalError(
                "Expected a statement of type IF, ASSIGN, WHILE, REXP, or RETURN, got {}".format(
                    str(type(statement))))

    def get_expression_used(self, expression):
        """
        Get a list of all of the variable_locations used by an expression
        :param expression: Some expression
        :return: A list of ast.Location objects
        """
        locations = list()
        if type(expression) is ast.ExpressionBare and type(expression.value) is ast.Location:
            locations.append(expression.value)
        for e in expression.children():
            locations += self.get_expression_used(e)
        return locations


def get_initialized(statements, preinitialized):
    ret = InitializedVariables(preinitialized, [])
    givs = GetInitializedVariables(statements)
    found = givs.run()
    ret.add_iv(found)
    ret.remove_redundant()
    return ret


class GetInitializedVariables:
    def __init__(self, statements, get_attributes=False):
        self.statements = statements
        self.get_attributes = get_attributes
        # We use the list constructor to get a copy of the list instead of a reference
        self.initialized_variables = InitializedVariables()

    def run(self):
        for statement in self.statements:
            iv = self.get_statement_ivs(statement)
            self.initialized_variables.add_iv(iv)
        self.initialized_variables.remove_redundant()
        return self.initialized_variables

    def get_statement_ivs(self, statement):
        """
        Given any statement, determine the variables possibly and definitely initialized.
        Notice that this code has special handling for IfStatements and when it encounters one it will continue to
        absorb all elif and else statement blocks to try to determine if a variable is declared within all of those
        blocks.
        :param statement: A statement to be checked (may have inner nested statements, e.g. while, if-else)
        :return: a tuple with two lists, the
        """
        if type(statement) is ast.StatementIf:
            return self.get_if_statement_ivs(statement)
        elif type(statement) is ast.StatementWhile:
            return self.get_while_statement_ivs(statement)
        elif type(statement) is ast.StatementAssign:
            if self.get_attributes:
                return self.get_assign_statement_att(statement)
            else:
                return self.get_assign_statement_ivs(statement)
        elif type(statement) is ast.StatementExp or type(statement) is ast.StatementRet:
            return InitializedVariables()
        else:
            raise err.QuackInternalError(
                "Expected a statement of type IF, ASSIGN, WHILE, REXP, or RETURN, got {}".format(str(type(statement))))

    def get_statement_list_ivs(self, statement_list):
        """
        Given a list of statements, make a recursive call to the GetInitializedVariables executable class.
        :param statement_list: A list of statements
        :return: An InitializedVariable structure
        """
        get_ivs = GetInitializedVariables(statement_list)
        return get_ivs.run()

    def get_if_statement_ivs(self, smnt):
        # Now we need to walk through the code blocks. We will mimic an intersection behavior,
        # The rule is that an initialization will be counted iff all of the code blocks have that initialization.
        # We get the variables initialized within every block.
        # We get the intersection of all of those initialized variables, that is the initialized variables
        # Anything that does not fall into that intersection is instead a possibly initialized variable
        ivs = self.get_statement_list_ivs(smnt.block.statements)
        if smnt.else_block:
            ivs.intersection(self.get_statement_list_ivs(smnt.else_block.statements))
        for elif_statement in smnt.eliflist:
            ivs.intersection(self.get_statement_list_ivs(elif_statement.block.statements))
        return ivs

    def get_while_statement_ivs(self, smnt):
        # Make a recursive call to check each of the statements within the while loop
        ivs = self.get_statement_list_ivs(smnt.block.statements)
        # Anything initialized in a while loop is at most possibly_initialized
        return InitializedVariables([], ivs.initialized + ivs.possibly_initialized)

    def get_assign_statement_ivs(self, smnt):
        loc = smnt.location
        while loc.path_exp:
            if type(loc.path_exp) is ast.ExpressionBare:
                loc = loc.path_exp.value
            else: # We assume that the only other type is ExpressionInvoke
                loc = loc.path_exp
        name = loc.ident
        declared_type = smnt.type_ident
        var = sym.VariableContext(name, declared_type)
        return InitializedVariables([var])

    def get_assign_statement_att(self, smnt):
        loc = smnt.location
        # Check if it is of the form "this.x" or "this.asdf", and not "this.foo().x" or "
        if type(loc.path_exp) is ast.ExpressionBare and loc.path_exp.value.ident == 'this':
            name = loc.ident
            declared_type = smnt.type_ident
            var = sym.VariableContext(name, declared_type)
            return InitializedVariables([var])
        return InitializedVariables()


def get_declared_attributes(statements):
    givs = GetInitializedVariables(statements, True)
    givs.run()
    return givs.initialized_variables.initialized


# class GetInitializedVariables:
#     def __init__(self, statements, args):
#         self.statements = statements
#         # We use the list constructor to get a copy of the list instead of a reference
#         self.initialized_variables = InitializedVariables()
#         self.initialized_variables.initialized += args
#
#     def run(self):
#         for statement in self.statements:
#             iv = self.get_statement_ivs(statement)
#             self.initialized_variables.add_iv(iv)
#         self.initialized_variables.remove_redundant()
#         return self.initialized_variables
#
#     def get_statement_ivs(self, statement):
#         """
#         Given any statement, determine the variables possibly and definitely initialized.
#         Notice that this code has special handling for IfStatements and when it encounters one it will continue to
#         absorb all elif and else statement blocks to try to determine if a variable is declared within all of those
#         blocks.
#         :param statement: A statement to be checked (may have inner nested statements, e.g. while, if-else)
#         :return: a tuple with two lists, the
#         """
#         if type(statement) is ast.StatementIf:
#             return self.get_if_statement_ivs(statement)
#         elif type(statement) is ast.StatementWhile:
#             return self.get_while_statement_ivs(statement)
#         elif type(statement) is ast.StatementExp:
#             return self.get_rexp_statement_ivs(statement)
#         elif type(statement) is ast.StatementAssign:
#             return self.get_assign_statement_ivs(statement)
#         elif type(statement) is ast.StatementRet:
#             return self.get_return_statement_ivs(statement)
#         else:
#             raise err.QuackInternalError(
#                 "Expected a statement of type IF, ASSIGN, WHILE, REXP, or RETURN, got {}".format(str(type(statement))))
#
#     def get_statement_list_ivs(self, statement_list):
#         """
#         Given a list of statements, make a recursive call to the GetInitializedVariables executable class.
#         :param statement_list: A list of statements
#         :return: An InitializedVariable structure
#         """
#         get_ivs = GetInitializedVariables(statement_list, self.initialized_variables)
#         return get_ivs.run()
#
#     def get_if_statement_ivs(self, smnt):
#         # Walk through all of the condition expressions, getting all of the variables used.
#         # example:
#         # if cexp1 {} elif cexp2 {} elif cexp3 else {}
#         #    ^             ^             ^
#         condition_used = self.get_used(smnt.condition_exp)
#         for elif_smnt in smnt.eliflist:
#             condition_used += self.get_used(elif_smnt.condition_exp)
#         for used in condition_used:
#             self.assert_initialized(used)
#
#         # Now we need to walk through the code blocks. We will mimic an intersection behavior,
#         # The rule is that an initialization will be counted iff all of the code blocks have that initialization.
#         # We get the variables initialized within every block.
#         # We get the intersection of all of those initialized variables, that is the initialized variables
#         # Anything that does not fall into that intersection is instead a possibly initialized variable
#         ivs = self.get_statement_list_ivs(smnt.block.statements)
#         if smnt.else_block:
#             ivs.intersection(self.get_statement_list_ivs(smnt.else_block.statements))
#         for elif_statement in smnt.eliflist:
#             ivs.intersection(self.get_statement_list_ivs(elif_statement.block.statements))
#         return ivs
#
#     def get_while_statement_ivs(self, smnt):
#         # check the conditional expression
#         condition_used = self.get_used(smnt.condition_exp)
#         for used in condition_used:
#             self.assert_initialized(used)
#
#         # Then make a recursive call to check each of the statements within the while loop
#         ivs = self.get_statement_list_ivs(smnt.block.statements)
#         # Anything initialized in a while loop is at most possibly_initialized
#         return InitializedVariables([], ivs.initialized + ivs.possibly_initialized)
#
#     def get_assign_statement_ivs(self, smnt):
#         used = self.get_used(smnt.expression)
#         for u in used:
#             self.assert_initialized(u)
#
#         name = smnt.location.ident
#         declared_type = smnt.type_ident
#         var = sym.VariableContext(name, declared_type)
#         return InitializedVariables([var])
#
#     def get_rexp_statement_ivs(self, smnt):
#         used = self.get_used(smnt.exp)
#         for u in used:
#             self.assert_initialized(u)
#         return InitializedVariables()
#
#     def get_return_statement_ivs(self, smnt):
#         if smnt.return_exp:
#             used = self.get_used(smnt.return_exp)
#             for u in used:
#                 self.assert_initialized(u)
#         return InitializedVariables()
#
#     def get_used(self, expression):
#         return get_expression_locations(expression)
#
#
# def assert_initialized(var_location, initialized_variables):
#     # TODO we need to make an seperate check that any attribute accesses happen on the correct object.
#     # This code assumes that any path accesses are accessing only objects of 'this' type
#     if not initialized_variables.is_initialized(var_location):
#         raise err.QuackCheckError("Usage of uninitialized variable {}".format(var_location), var_location.linespan)

