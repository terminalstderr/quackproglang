import symtable as sym
import ivs as givs
from staticchecker_helper import *
from overridechecker import typecheck_methodoverride


def build_symbol_table(ast_program):
    symbol_table = build_skeleton_symbol_table(ast_program)
    build_attribute_tables(symbol_table)
    build_constructors(symbol_table)
    build_skeleton_method_tables(symbol_table)
    fill_method_tables(symbol_table)
    infer_variable_types(symbol_table)
    return symbol_table


def run_checks(symbol_table):
    check_undefined_method_call(symbol_table)
    check_undefined_constructor_call(symbol_table)
    check_attempt_public_access(symbol_table)
    check_undefined_attribute_usage(symbol_table)
    check_uninitialized_variable_usage(symbol_table)
    check_method_overriding(symbol_table)
    check_type_conditional_expressions(symbol_table)
    check_type_method_invocation(symbol_table)
    check_type_return(symbol_table)
    check_type_path_expressions(symbol_table)


def build_skeleton_symbol_table(ast_program):
    """
    All this function should do is build a skeleton that has all of the classes in it.
    Preconditions: None
    Postconditions: Should be able to run the following methods,
        cls = find_class(cls_ident)
        lca_ident = cls.lca(other)
        cls.is_subclass_of(other)
        cls.is_superclass_of(other)

    :param ast_program:
    :return:
    """
    return sym.build_symbol_table(ast_program)


def build_attribute_tables(symbol_table):
    """
    This function fills out the class attributes/fields/private variables. The only thing that will change is that all
    of the st_node.class_context.attribute_table data structures will be filled.
    Can check the name and declared_type of each attribute for correctness. The order should match the order in which
    the variables were declared.
    :param symbol_table: A skeleton symbol table that has been built using build_skeleton_symbol_table()
    :return: None
    """
    for stnode in symbol_table.as_list():
        stnode.class_context.construct_attribute_table()


def build_constructors(symbol_table):
    for stnode in symbol_table.as_list():
        stnode.class_context.construct_constructor()


def build_skeleton_method_tables(symbol_table):
    for stnode in symbol_table.as_list():
        if stnode.parent:
            stnode.class_context.construct_skeleton_method_table(stnode.parent.class_context.method_table)
        else:
            stnode.class_context.construct_skeleton_method_table([])


def fill_method_tables(symbol_table):
    for stnode in symbol_table.as_list():
        # Do it for all methods, including the constructor
        for method in stnode.class_context.method_table:
            method.construct_variable_table()
        constructor = stnode.class_context.constructor
        constructor.construct_variable_table()


def infer_variable_types(symbol_table):
    for stnode in symbol_table.as_list():
        # Do it for all methods, including the constructor
        for method in stnode.class_context.method_table:
            for variable in method.variable_table:
                variable.inferred_type = variable.declared_type
        constructor = stnode.class_context.constructor
        for variable in constructor.variable_table:
            variable.inferred_type = variable.declared_type
        for attribute in stnode.class_context.attribute_table:
            attribute.inferred_type = attribute.declared_type


def check_undefined_method_call(symbol_table):
    # Build up our checker mechanism, will check that:
    #   every invoke expression's method_ident is in this class's method table
    def check(e, ivs):
        # Figure out the type of the expression
        t = typeof(e.path_exp, symbol_table, ivs)
        c = find_class_throw(symbol_table, t, e.linespan)
        find_method_throw(c, e.method_ident, e.linespan)
    checker = Checker.init_single(check, [ast.ExpressionInvoke])

    for stnode in symbol_table.as_list():
        # Run the checker for each method and the constructor!
        for method in stnode.class_context.method_table:
            checker.run(method.ast_statements, method.variable_table)
        constructor = stnode.class_context.constructor
        checker.run(constructor.ast_statements, constructor.variable_table)

        
        
def check_undefined_constructor_call(symbol_table):
    # Build up our checker mechanism, will check that:
    #   every invoke expression's method_ident is in this class's method table
    def check(e, ivs):
        # Figure out the type of the expression
        find_class_throw(symbol_table, e.ident, e.linespan)
    checker = Checker.init_single(check, [ast.ExpressionConstructor])

    for stnode in symbol_table.as_list():
        # Run the checker for each method and the constructor!
        for method in stnode.class_context.method_table:
            checker.run(method.ast_statements, method.variable_table)
        constructor = stnode.class_context.constructor
        checker.run(constructor.ast_statements, constructor.variable_table)



def check_undefined_attribute_usage(symbol_table):
    def check_attrnames_in_attrtable(aname_linespan_list, attr_table, cls_ident):
        for attr, linespan in aname_linespan_list:
            if attr not in [a.name for a in attr_table]:
                raise err.QuackCheckError("Used nonexistant attribute '{}' in class '{}'".format(attr, cls_ident),
                                          linespan)

    for stnode in symbol_table.as_list():
        # Do it for all methods, including the constructor
        for method in stnode.class_context.method_table:
            name_lspan_list = givs.GetUsedVariables.get_used_attribute_name_linespan_list(method.ast_statements)
            check_attrnames_in_attrtable(name_lspan_list, stnode.class_context.attribute_table, stnode.ident)
        constructor = stnode.class_context.constructor
        name_lspan_list = givs.GetUsedVariables.get_used_attribute_name_linespan_list(constructor.ast_statements)
        check_attrnames_in_attrtable(name_lspan_list, stnode.class_context.attribute_table, stnode.ident)


def check_attempt_public_access(symbol_table):
    for stnode in symbol_table.as_list():
        # Build up our checker mechanism, will check that:
        #   every Location access has a LHS of this class type
        def check(e, ivs):
            # Figure out the type of the expression
            if e.path_exp:
                t = typeof(e.path_exp, symbol_table, ivs)
                if t != stnode.ident:
                    c = symbol_table.find_class(t)
                    if c:
                        msg = "Attempted to access protected attribute '{}' from class '{}'".format(e.ident, c.ident)
                    else:
                        msg = "Attempt to access non-existant attribute '{}'".format(e.ident)
                    raise err.QuackCheckError(msg, e.linespan)
        checker = Checker.init_single(check, [ast.Location])

        # Run the checker for each method and the constructor!
        for method in stnode.class_context.method_table:
            checker.run(method.ast_statements, method.variable_table)
        constructor = stnode.class_context.constructor
        checker.run(constructor.ast_statements, constructor.variable_table)


def check_uninitialized_variable_usage(symbol_table):
    def check_varnames_in_vtable(vname_linespan_list, vtable):
        for vname, linespan in vname_linespan_list:
            if vname not in [v.name for v in vtable]:
                raise err.QuackCheckError("Variable '{}' used without being initialized".format(vname), linespan)

    for stnode in symbol_table.as_list():
        # Do it for all methods, including the constructor
        for method in stnode.class_context.method_table:
            name_lspan_list = givs.GetUsedVariables.get_used_variable_name_linespan_list(method.ast_statements)
            check_varnames_in_vtable(name_lspan_list, method.variable_table)
        constructor = stnode.class_context.constructor
        name_lspan_list = givs.GetUsedVariables.get_used_variable_name_linespan_list(constructor.ast_statements)
        check_varnames_in_vtable(name_lspan_list, constructor.variable_table)


def check_method_overriding(symbol_table):
    for stnode in symbol_table.as_list():
        parent_methods = stnode.class_context.method_table
        for child_stnode in stnode.children:
            child_methods = child_stnode.class_context.method_table
            for i in range(len(parent_methods)):
                typecheck_methodoverride(parent_methods[i], child_methods[i], symbol_table)


def check_type_conditional_expressions(symbol_table):
    # Build up our checker mechanism, will check that:
    #   every statement that contains a conditional expression, that conditional expression has typeof boolean
    def check_cond(s, ivs):
        # Figure out the type of the expression
        t = typeof(s.condition_exp, symbol_table, ivs)
        if t != ast.QuackBoolean.signature.ident:
            raise err.QuackCheckError("Non-Boolean value '{}' passed as condition"
                                      "".format(s.condition_exp), s.condition_exp.linespan)
    checker_cond = Checker.init_single(check_cond, [ast.StatementIf, ast.SubStatementElif, ast.StatementWhile])

    # Build up our checker mechanism, will check that:
    #   'and' and 'or' boolean expressions have arguments of type boolean
    def check_andor(e, ivs):
        # Figure out the type of the expression
        t = typeof(e.exp1, symbol_table, ivs)
        if t != ast.QuackBoolean.signature.ident:
            raise err.QuackCheckError("Non-Boolean value '{}' was passed as subexpression to and/or"
                                      "".format(e.exp1), e.exp1.linespan)
        t = typeof(e.exp2, symbol_table, ivs)
        if t != ast.QuackBoolean.signature.ident:
            raise err.QuackCheckError("Non-Boolean value '{}' was passed as subexpression to and/or"
                                      "".format(e.exp2), e.exp2.linespan)
    checker_andor = Checker.init_single(check_andor, [ast.ExpressionAnd, ast.ExpressionOr])

    # Build up our checker mechanism, will check that:
    #   'not' boolean expressions have argument of type boolean
    def check_not(e, ivs):
        # Figure out the type of the expression
        t = typeof(e.exp, symbol_table, ivs)
        if t != ast.QuackBoolean.signature.ident:
            raise err.QuackCheckError("Non-Boolean value '{}' was passed as subexpression to not"
                                      "".format(e.exp), e.exp.linespan)
    checker_not = Checker.init_single(check_not, [ast.ExpressionNot])

    def check_all(statements, ivs):
        checker_not.run(statements, ivs)
        checker_andor.run(statements, ivs)
        checker_cond.run(statements, ivs)

    for stnode in symbol_table.as_list():
        # Run the checker for each method and the constructor!
        for method in stnode.class_context.method_table:
            check_all(method.ast_statements, method.variable_table)
        constructor = stnode.class_context.constructor
        check_all(constructor.ast_statements, constructor.variable_table)


def check_type_method_invocation(symbol_table):
    # Build up our checker mechanism, will check that:
    #   every invoke expression's arguments have correct type
    def check(e, ivs):
        # Need access to the methods of this class, and
        t = typeof(e.path_exp, symbol_table, ivs)
        c = find_class_throw(symbol_table, t, e.linespan)
        mthd = find_method_throw(c, e.method_ident, e.linespan)
        for i, invoke_arg, formal_arg in zip(range(len(e.invoke_args)), e.invoke_args, mthd.arguments):
            observed_type = typeof(invoke_arg, symbol_table, ivs)
            observed_cls = symbol_table.find_class_stnode(observed_type)
            expected_cls = symbol_table.find_class_stnode(formal_arg.declared_type)
            if not observed_cls.is_subclass_of(expected_cls):
                msg = "Invoked method {}::{}(...) with argument position {} expected object of type {} but was type {}"\
                      "".format(c.ident, mthd.name, i+1, formal_arg.declared_type, observed_type)
                raise err.QuackCheckError(msg, invoke_arg.linespan)

    checker = Checker.init_single(check, [ast.ExpressionInvoke])

    for stnode in symbol_table.as_list():
        # Run the checker for each method and the constructor!
        for method in stnode.class_context.method_table:
            checker.run(method.ast_statements, method.variable_table)
        constructor = stnode.class_context.constructor
        checker.run(constructor.ast_statements, constructor.variable_table)


def check_type_return(symbol_table):
    # Walk method, get all return statements
    # Build up our checker mechanism, will check that:
    #   For every return statement, ensure that the typeof(s.exp) is return statement
    def check(s, meth):
        ivs = meth.variable_table
        ret_type = meth.return_type
        # Figure out the type of the expression
        if s.return_exp:
            observed_type = typeof(s.return_exp, symbol_table, ivs)
        else:
            observed_type = ast.QuackNothing.signature.ident
        observed_cls = symbol_table.find_class_stnode(observed_type)
        expected_cls = symbol_table.find_class_stnode(ret_type)
        if not observed_cls.is_subclass_of(expected_cls):
            msg = "Method {}::{}(...) expects return expression of type {} but got return type {}" \
                  "".format(meth.class_ident, meth.name, ret_type, observed_type)
            raise err.QuackCheckError(msg, s.linespan)
    checker = Checker.init_single(check, [ast.StatementRet])

    for stnode in symbol_table.as_list():
        # Run the checker for each method and the constructor!
        for method in stnode.class_context.method_table:
            checker.run(method.ast_statements, method)
        constructor = stnode.class_context.constructor
        checker.run(constructor.ast_statements, constructor)


def check_type_path_expressions(symbol_table):
    # All assignment statements that have a path-exp, the path-exp must be of 'this-class' type
    # Walk method, get all return statements
    # Build up our checker mechanism, will check that:
    #   For every return statement, ensure that the typeof(s.exp) is return statement
    def check(e, meth):
        if not e.path_exp:
            # this means that it is a local variable
            return
        ivs = meth.variable_table
        this_type = meth.class_ident
        observed_type = typeof(e.path_exp, symbol_table, ivs)
        observed_cls = symbol_table.find_class_stnode(observed_type)
        expected_cls = symbol_table.find_class_stnode(this_type)
        if not observed_cls.is_subclass_of(expected_cls):
            msg = "{}::{}(...) expect path to be of type {} but found object of type {}" \
                  "".format(meth.class_ident, meth.name, this_type, observed_type)
            raise err.QuackCheckError(msg, e.linespan)
    checker = Checker.init_single(check, [ast.Location])

    for stnode in symbol_table.as_list():
        # Run the checker for each method and the constructor!
        for method in stnode.class_context.method_table:
            checker.run(method.ast_statements, method)
        constructor = stnode.class_context.constructor
        checker.run(constructor.ast_statements, constructor)

