"""
err: Error
"""
import ast


class QuackError(Exception):
    def __init__(self, msg, linespan=(-1, -1)):
        self.msg = msg
        self.linespan = linespan
        super().__init__(msg)

    def pretty(self):
        if self.linespan[0] == -1 or self.linespan[0] == -1:
            return "N/A: {}".format(self.msg)
        elif self.linespan[0] == self.linespan[1]:
            return "{}: {}".format(self.linespan[0], self.msg)
        else:
            return "{}-{}: {}".format(self.linespan[0], self.linespan[1], self.msg)


class QuackCheckError(QuackError):
    pass


class QuackInternalError(QuackError):
    pass


class QuackGenerationError(QuackError):
    pass


# This is a useful assertion since we will occasionally have to assume a certain type of node
def assert_node_type(node, expected_node_type):
    if not isinstance(node, ast.Node):
        raise QuackInternalError("Assumed type {} but found type {}".format(expected_node_type, type(node)))
    if not isinstance(node, expected_node_type):
        raise QuackInternalError("Assumed type {} but found type {}".format(expected_node_type, type(node)),
                                 node.linespan)


def assert_insert_success(insert_success):
    if not insert_success:
        raise QuackInternalError("Could not insert an Internal class")


def assert_same(t1, t2):
    if t1 != t2:
        raise QuackCheckError("Expected same type but was different types!")
