"""
chk: Checker and Checks

Can only access fields/attributes of same class (not subclass)

1. How to determine if a location is a subtype of cls_id:
    Assume given an AST Location 'var_loc'.
    Assume given a Class identity 'cls_id'.

    lca = least_common_ancestor(typeof(var_loc), cls_id)
    assert lca == cls_id

1. Potential use of a uninitialized variable on any program execution path
2. Potential type error:
    The type of the test in an 'if', 'elif', or 'while' must be a subtype of Boolean.
    Most other type errors will typically show up either as an actual argument type that is not a subtype of the corresponding actual argument, an actual argument list that is too long or short, or a method not found in a class.
    The last is particularly likely when the type of the receiver object is not correct, e.g., when the static type of variable x is "above" the class that has the desired method.
3. Potential call of undefined method
4.DONE - Illegal redefinition of a name that is in scope (e.g., creating a variable x where a variable x is already in scope)
5. Method returns wrong type  (includes ending without returning)
6. Incompatible overridden method  (should have same number of arguments, each formal argument a supertype of overridden method, return type a subtype of returned type of overridden method)


1. Classes cannot be redefined.
2. Methods cannot be redefined in the same class.
3. A subclass may override a method definition from one of its superclasses, but the overriding method must be compatible.
    Meaning it has the same number of arguments and each argument is a superclass of the corresponding argument of the superclass.
    The return type must be subclass of the return type of the overridden method.
4. If the superclass has a field x of type c, then the subclass must initialize field x to a value which is some subclass of c.
5. Each method must return the type it is declared to return, on all execution paths.
6. Each variable must be initialized before it is used, on all syntactically possible execution paths.
    ("Syntactically possible" means, for example, that we must consider every control flow path, even if some can never happen because of data values.)
7. Each method call is well-typed with respect to the static type of the receiver object.
    For example, in x.foo(y), x is the receiver object;  x must have a method called foo taking one argument, and
    the actual (static) type of y must be a subtype of the declared type of the formal argument.  This check is made
    while performing type inference.
8. Ensure that conditional_exp are always  boolean (while and if statements)


# TODO Checks to perform over AST:
# DONEZO 1. Potential use of an unitialized variable on any program execution path
# 2. Potential type error
# 3. Potential call of undefined method
# 4. Illegal redef of a name that is in scope
# 5. Method returns wrong type
# 6. Incompatible overridden method


3. Check that inferred type is subtype of declared type
"""
import ast
import err

symbol_table = None


def check_return_statements(node):
    if type(node) is ast.Method:
        ret_type = node.return_type_ident
        for statement in node.block.statements:
            if type(statement) is ast.StatementRet:
                if ret_type:
                    assert_expression_type(statement.return_exp, ret_type)
                else:
                    if statement.return_exp:
                        msg = "Return expression {} when method expects return of Nothing".format(statement.return_exp)
                        raise err.QuackCheckError(msg, statement.return_exp.linespan)


def assert_expression_type(expr, type_ident):
    if symbol_table.typeof(expr) != type_ident:
        raise err.QuackCheckError("Expression '{}' is not of type {}".format(expr, type_ident), expr.linespan)


def check_conditional_expressions(node):
    if type(node) is ast.StatementWhile:
        assert_expression_boolean(node.condition_exp)
    elif type(node) is ast.StatementIf:
        assert_expression_boolean(node.condition_exp)
    elif type(node) is ast.SubStatementElif:
        assert_expression_boolean(node.condition_exp)


def assert_expression_boolean(expression):
    assert_expression_type(expression, ast.QuackBoolean.signature.ident)


def check_valid_constructor(node):
    if type(node) is ast.ExpressionConstructor:
        if not symbol_table.find_class(node.ident):
            return "Could not find matching constructor for: " + node.ident


# def check_type_single_pass(node):
#     # Need to look at every
#     """
#     MYoung
#     If the inferred type is not a subtype of the declared type, it's a type error
#     If the inferred type is a subtype of the declared type, the static type is the declared type
#     """
#     # typeof needs a
#     if type(node) is ast.StatementAssign:
#         var = symbol_table.find_variable(node.location)
#         inferred_type = symbol_table.typeof(node.expression)
#         # Get the least common ancestor!
#         # TODO: Calculated type MUST be a subtype of the declared type (this is much different than current implementation)
#         # TODO: we should be getting the LCS of inferred type against inferred type!
#         if inferred_type and var.declared_type is not "unknown":
#             cls2 = symbol_table.find_class(var.declared_type)
#             cls1 = symbol_table.find_class(inferred_type)
#             lca = cls1.find_common_ancestor(cls2)
#             var.inferred_type = lca
#         # Get the inferred type
#         elif inferred_type:
#             var.inferred_type = inferred_type
#         else:
#             # if the type cannot be inferred, then don't modify the variable
#             pass


def check_valid_method(node):
    if type(node) is ast.ExpressionInvoke:
        if not symbol_table.find_method(node):
            raise err.QuackCheckError("Could not find method '{}' at '{}'".format(node.method_ident, node),
                                      node.linespan)


# # TODO: This is done more thoroughly in the typechecker
# def check_decleration_before_usage(node):
#     if type(node) is ast.Program:
#         get_ivs = ivz.GetInitializedVariables(node.statements)
#     elif type(node) is ast.Method:
#         get_ivs = ivz.GetInitializedVariables(node.block.statements)
#     # Any initialized
#     elif type(node) is ast.Class:
#         get_ivs = ivz.GetInitializedVariables(node.body.statements)
#     else:
#         return
#     get_ivs.run()
#     print(get_ivs)


# Checks for a constructor call with no matching constructor
def check_unknown_constructor(tree, node):
    if not node:
        return None
    if type(node) is ast.ExpressionConstructor:
        if not tree.find_class_stnode(node.ident):
            return node.ident
    for child in node.children():
        ret = check_unknown_constructor(tree, child)
        if ret:
            return ret
    return None


class QuackChecker:
    def __init__(self):
        self.checks = []

    # Walks the tree in a top-down fashion, running the check on each node.
    # If the check ever returns (anything), the walk will be cancelled and the value
    # returned from check will be passed back up the stack.
    def check_top_down(self, node):
        for check in self.checks:
            error = check(node)
            if error:
                return error
        for child in node.children():
            error = self.check_top_down(child)
            if error:
                return error

    def add_check(self, check):
        self.checks.append(check)

    def run(self, ast_root):
        err = self.check_top_down(ast_root)
        if err:
            print("Error when checking! " + str(err))


# def type_infer(ivs, )
#
# def type_check(ivs, sym_table, node):
#
#     for child in node.children():
#         error = self.check_top_down(child)
#         if error:
#             return error
#

