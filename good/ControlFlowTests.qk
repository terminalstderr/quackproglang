/*
Ryan Leonard:
ControlFlowTests.qk demonstrates successful use of control flow and short circuit evaluation
*/


class Test() {
  def run() { }
}


class TestCountdown() extends Test {
  def run() {
    ("EXPECTED: Countdown from 10 to liftoff \n").PRINT();
    i : Int = 10;
    while i > 0
    {
      tmp: String = i.STR() + ", ";
      tmp.PRINT();
      i = i - 1;
    }
    "liftoff!\n".PRINT();
  }
}


class TestWhileFalse() extends Test {
  def run() {
    t: Boolean = true;
    f: Boolean = false;
    ("EXPECTED: should see 0 'ERROR's\n").PRINT();

    while false and true
    {
      "ERROR\n".PRINT();
    }

    while f or false
    {
      "ERROR\n".PRINT();
    }

    while f and 100 > 10
    {
      "ERROR\n".PRINT();
    }
  }
}


class TestShortcircuit() extends Test {
  def run() {
    ("EXPECTED: should see 0 'SideEffect's\n").PRINT();

    while false and this.true_se()
    {}
    if true or this.false_se()
    {}

    e : Boolean = true or this.true_se();
    e = false and this.true_se();
    e = true or not this.true_se();

    d: Int = 0;
    n: Int = 13;
    e = not (d == 0) and (this.divide_by(n,d) > 1);
  }

  def divide_by(n: Int, d: Int) : Int {
    if d == 0
    {
      "SideEffect: Divide-by-zero\n".PRINT();
    }
    return n/d;
  }

  def true_se() : Boolean 
  {
    "SideEffect\n".PRINT();
    return true;
  }

  def false_se() : Boolean 
  {
    "SideEffect\n".PRINT();
    return false;
  }
}


class TestDayActivity() extends Test {
  def run() {
    ("EXPECTED: next 4 should be 'food with parent'\n").PRINT();
    this.make_decision("SUNNY", true, 5);
    this.make_decision("SUNNY", true, 50);
    this.make_decision("WINDY", true, 100);
    this.make_decision("SUNNY", true, 200);

    ("EXPECTED: play tennis'\n").PRINT();
    this.make_decision("SUNNY", false, 10);
    ("EXPECTED: play video games'\n").PRINT();
    this.make_decision("RAINY", false, 2);
    ("EXPECTED: play video games'\n").PRINT();
    this.make_decision("RAINY", false, 20);
    ("EXPECTED: drink beer'\n").PRINT();
    this.make_decision("RAINY", false, 100);
    ("EXPECTED: fly a kite'\n").PRINT();
    this.make_decision("WINDY", false, 100);
  }

  def make_decision(weather: String, parent_in_town: Boolean, money: Int) 
  {
    weather == weather;
    if parent_in_town
    {
      if money > 10
      {
        "Ask to go out for food with parent\n".PRINT();
      } 
      else
      {
        "Beg to go out for food with parent\n".PRINT();
      }
    }
    else 
    {
      if weather == "SUNNY"
      {
        "Play tennis\n".PRINT();
      }
      elif weather == "WINDY"
      {
        "Fly a kite\n".PRINT();
      }
      elif weather == "RAINY"
      {
        if money > 50
        {
          "Go out for beer\n".PRINT();
        }
        elif money > 20
        {
          "Go out for coffee\n".PRINT();
        }
        else
        {
          "Play video games\n".PRINT();
        }
      }
    } 
  }
}


"\nCountdown Test\n".PRINT();
t: Test = TestCountdown();
t.run();

"\nWhile(false) Test\n".PRINT();
t: Test = TestWhileFalse();
t.run();

"\nShort Circuit Test\n".PRINT();
t: Test = TestShortcircuit();
t.run();

"\nDecision Tree Test\n".PRINT();
t: Test = TestDayActivity();
t.run();

